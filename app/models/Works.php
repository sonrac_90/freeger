<?php

/**
 * This is the model class for table "{{works}}".
 *
 * The followings are the available columns in table '{{works}}':
 * @property integer $id
 * @property integer $visible
 * @property integer $position
 * @property string $title
 * @property string $description
 * @property string $link
 *
 * The followings are the available model relations:
 * @property ImgWorks[] $imgWorks
 */
class Works extends CActiveRecord
{
    public $max;
    public static $path = '/shared/uploads/works/';
    public $image;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{works}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, link', 'required'),
			array('title, description, link', 'length', 'max'=>255),
			array('position, visible', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, description, link, image, position, visible', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imgWorks' => array(self::HAS_MANY, 'ImgWorks', 'works_id', 'order' => 'imgWorks.position ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'link' => 'Link',
			'visible' => 'Visible',
			'position' => 'Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->with     = 'imgWorks';
        $criteria->together = false;

        if (!isset($_REQUEST['Works_sort']))
            $criteria->order = "t.position ASC";//, imgWorks.position ASC";


		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.link',$this->link,true);
		$criteria->compare('t.position',$this->position,true);
		$criteria->compare('t.visible',$this->visible,true);
        $criteria->compare('imgWorks.name', $this->image, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 100
            )

		));
	}

    public function beforeSave()
    {
        parent::beforeSave();
        return true;
    }

    public function afterSave()
    {
        parent::afterSave();
        return true;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Works the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
