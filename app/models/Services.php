<?php

/**
 * This is the model class for table "{{services}}".
 *
 * The followings are the available columns in table '{{services}}':
 * @property integer $id
 * @property integer $visible
 * @property string $text
 * @property string $icons_png
 * @property string $icons_svg
 * @property string $position
 * @property string $title
 */
class Services extends CActiveRecord
{
    public $max;
    static $path = '/shared/uploads/services/';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{services}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text, title', 'required'),
			array('id, position, visible', 'numerical', 'integerOnly'=>true),
			array('text, icons_png, icons_svg, title', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, text, icons_png, icons_svg, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'text' => 'About Services',
			'icons_png' => 'Icon PNG',
			'icons_svg' => 'Icon SVG',
			'position' => 'Position',
            'title' => 'Title',
            'visible' => 'Visible'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        if (!isset($_REQUEST['Services_sort']))
            $criteria->order = "position";

		$criteria->compare('id',$this->id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('icons_png',$this->icons_png,true);
		$criteria->compare('icons_svg',$this->icons_svg,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('visible',$this->visible,true);

		return new CActiveDataProvider($this, array(
            'pagination'=>array(
                'pageSize'=>50,
            ),
            'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Services the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
