<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1, width=device-width" />
    <link href="<?=Yii::app()->getBaseUrl(true)?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <title>ArOnce Creative</title>

    <script type="text/javascript">
        var baseUrl = '<?=Yii::app()->getBaseUrl(true)?>';
        var CURRENT_PAGE = '<?=Yii::app()->controller->id?>';
        var CURRENT_WORK_ID = '<?=Yii::app()->request->getParam('id', '')?>';
    </script>

    <link rel="stylesheet" href="<?=Yii::app()->getBaseUrl(true)?>/media/css/main.css" type="text/css" />
    <link rel="stylesheet" href="<?=Yii::app()->getBaseUrl(true)?>/media/css/media.css" type="text/css" />

    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/dvlp.js"></script>

    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/libs/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/libs/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/libs/jquery.touchscroll.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/libs/jquery.fontautosize.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/libs/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/libs/jquery.history.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/libs/jquery.resizer.js"></script>

    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/mouse_and_touch_scroll.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/touch_scroll.js?asd=<?=rand(0, 1000)?>"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/works_slider.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/services_slider.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/history.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/resizers.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/main_menu.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/get_data.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/preloader.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/media/js/main.js"></script>
</head>

<body>
    <?php echo $content; ?>
</body>

</html>