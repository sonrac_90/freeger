<div class="b-main_wrap asd-wrap">

    <div class="b-main_content asd-wrap">
        <div class="b-header asd-wrap">
            <div class="b-preloader_wrap asd-wrap">
                <div class="b-preloader asd-wrap"></div>
            </div>

            <div class="b-header_height asd-wrap">
                <div class="b-header_height_in asd-wrap">
                    <div class="b-wrap">
                        <div class="b-header_logo"><a href="javascript:set_page('home')"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/header-logo.png" /></a></div>

                        <div class="b-menu_button" onclick="main_menu_toggle();"><span></span><span></span><span></span></div>

                        <div class="b-menu b-menu_pc">
                            <div class="b-menu_item b-menu_item_selected"><a href="javascript:set_page('home')">Home</a></div>
                            <div class="b-menu_item"><a href="javascript:set_page('services')">Services</a></div>
                            <div class="b-menu_item"><a href="javascript:set_page('works')">Works</a></div>
                            <div class="b-menu_item"><a href="javascript:set_page('awards')">Clients & Awards</a></div>
                            <div class="b-menu_item"><a href="javascript:set_page('contacts')">Contacts</a></div>
                            <div class="asd-clear"></div>
                        </div>

                        <div class="asd-clear"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="b-content asd-wrap">
            <div id="wrap-home" class="b-content_in asd-wrap" style="top: 100%;"></div>
            <div id="wrap-services" class="b-content_in asd-wrap" style="top: 100%;"></div>
            <div id="wrap-awards" class="b-content_in asd-wrap" style="top: 100%;"></div>
            <div id="wrap-contacts" class="b-content_in asd-wrap" style="top: 100%;"></div>
            <div id="wrap-works" class="b-content_in asd-wrap" style="top: 100%;"></div>

            <?php
                $works = Works::model()->findAll(array('order' => 'id', 'condition' => 'visible = 1'));
                foreach ($works as $work)
                {
            ?>
                    <div id="wrap-work_id<?=$work->id?>" class="wrap-work_id b-content_in asd-wrap" data-work_id="<?=$work->id?>" style="top: 100%;"></div>
            <?php } ?>
        </div>
    </div>

    <div class="b-menu_content asd-wrap">
        <div class="b-header_logo"><a href="javascript:set_page('home')"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/header-logo.png" /></a></div>

        <div class="b-menu b-menu_mobile">
            <div class="b-menu_item b-menu_item_1 b-menu_item_selected"><a href="javascript:set_page('home')">Home</a></div>
            <div class="b-menu_item b-menu_item_2"><a href="javascript:set_page('services')">Services</a></div>
            <div class="b-menu_item b-menu_item_3"><a href="javascript:set_page('works')">Works</a></div>
            <div class="b-menu_item b-menu_item_4"><a href="javascript:set_page('awards')">Clients & Awards</a></div>
            <div class="b-menu_item b-menu_item_5"><a href="javascript:set_page('contacts')">Contacts</a></div>
            <div class="asd-clear"></div>
        </div>

        <?php if (isset($social_links)) { ?>
            <div class="b-socials">
                <?php foreach ($social_links as $index => $social_link) { ?>
                    <div class="b-social_item b-social_item_<?=($index+1)?>"><a href="<?=$social_link->link?>" target="_blank"><?=$social_link->name?></a></div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

</div>