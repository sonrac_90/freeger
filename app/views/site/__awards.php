<div class="b-content_bg asd-wrap">
    <img class="bg" src="<?=Yii::app()->getBaseUrl(true)?>/media/img/bg/clients.jpg" />
</div>
<div class="b-content_video asd-wrap" style="display: none;">
    <div class="b-content_video_in asd-wrap" data-src="<?=Yii::app()->getBaseUrl(true)?>/media/video/awards"></div>
</div>
<div class="b-content_overlay asd-overlay"></div>

<div class="asd-wrap b-clients_wrap">
    <div class="b-height_clients b-height_clients_1"></div>

    <div class="b-clients_award_wrap">
        <div class="b-clients_column b-clients_column_left">
            <div class="b-clients_award b-clients_award_1">
                <div class="b-clients_award_img"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/award-1.png" /></div>
                <div class="b-clients_award_content">
                    <!--<div class="b-clients_award_branch b-clients_award_branch_left"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/branch-left.png" /></div>
                <div class="b-clients_award_branch b-clients_award_branch_right"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/branch-right.png" /></div>-->
                    <div class="b-clients_award_text asd-wrap"><div class="asd-text-center-1"><div class="asd-text-center-2">Cannes<br/>Cyber Lions<br/>Finalist</div></div></div>
                </div>
            </div>
            <div class="b-clients_award b-clients_award_2">
                <div class="b-clients_award_img"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/award-2.png" /></div>
                <div class="b-clients_award_content">
                    <!--<div class="b-clients_award_branch b-clients_award_branch_left"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/branch-left.png" /></div>
                <div class="b-clients_award_branch b-clients_award_branch_right"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/branch-right.png" /></div>-->
                    <div class="b-clients_award_text asd-wrap"><div class="asd-text-center-1"><div class="asd-text-center-2">Webby Awards<br/>3 Golds</div></div></div>
                </div>
            </div>
        </div>

        <div class="b-height_clients b-height_clients_2"></div>

        <div class="b-clients_column b-clients_column_right">
            <div class="b-clients_award b-clients_award_3">
                <div class="b-clients_award_img"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/award-3.png" /></div>
                <div class="b-clients_award_content">
                    <!--<div class="b-clients_award_branch b-clients_award_branch_left"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/branch-left.png" /></div>
                <div class="b-clients_award_branch b-clients_award_branch_right"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/branch-right.png" /></div>-->
                    <div class="b-clients_award_text asd-wrap"><div class="asd-text-center-1"><div class="asd-text-center-2">The One Show<br/>Silver</div></div></div>
                </div>
            </div>
            <div class="b-clients_award b-clients_award_4">
                <div class="b-clients_award_img"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/award-4.png" /></div>
                <div class="b-clients_award_content">
                    <!--<div class="b-clients_award_branch b-clients_award_branch_left"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/branch-left.png" /></div>
                <div class="b-clients_award_branch b-clients_award_branch_right"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/branch-right.png" /></div>-->
                    <div class="b-clients_award_text asd-wrap"><div class="asd-text-center-1"><div class="asd-text-center-2">FWA<br/>Site of the Day<br/>15 times</div></div></div>
                </div>
            </div>
        </div>
    </div>

    <div class="b-height_clients b-height_clients_3"></div>

    <div class="b-clients_list">
        <div class="b-clients_list_item"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/clients-normal.png" /></div>
        <div class="b-clients_list_item_iphone_hor"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/clients-iphone-hor.png" /></div>
        <div class="b-clients_list_item_iphone_vert"><img src="<?=Yii::app()->getBaseUrl(true)?>/media/img/clients-iphone-vert.png" /></div>
    </div>
</div>