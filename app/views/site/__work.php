<?php
    /**
     * @var $work Works
     */

    $work_margin = '';
    $margin_array = array('B', 'D', 'E', 'F', 'H', 'P', 'R');
    $first_letter = strtoupper(substr(trim($work->title), 0, 1));

    if ($first_letter == 'A') {
        $work_margin = ' style="margin-left: 0.5%;" ';
    } else if (in_array($first_letter, $margin_array)) {
        $work_margin = ' style="margin-left: -1%;" ';
    } else {
        $work_margin = ' style="margin-left: -0.5%;" ';
    }
?>
<?php if (isset($work)) { ?>

    <div class="b-work_wrap asd-wrap">
        <div class="b-work_slides asd-wrap">
            <?php if (isset($work->imgWorks) && count($work->imgWorks) > 0) { ?>
                <?php
                    foreach ($work->imgWorks as $index => $img_work)
                    {
                        $slide_count = 'image-slide-'.($index+1);
                        $slide_src = Yii::app()->getBaseUrl(true).'/shared/uploads/works/'.$img_work->name;
                        $slide_not_loaded = '';
                        $slide_current = '';
                        $slide_style = '';

                        if ($index > 0) {
                            $slide_not_loaded = 'img-not-loaded';
                            $slide_style = 'style="z-index:-1;"';
                        } else {
                            $slide_current = 'image-slide-current';
                        }
                ?>
                        <div class="image-slide asd-wrap <?=$slide_count?> <?=$slide_current?> <?=$slide_not_loaded?>" data-src="<?=$slide_src?>" <?=$slide_style?>>
                            <?php if ($index < 0) { ?>
                                <img class="bg" src="<?=$slide_src?>">
                            <?php } ?>
                        </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="b-work_slides_grey asd-wrap">
            <?php if (isset($work->imgWorks) && count($work->imgWorks) > 0) { ?>
                <?php
                    foreach ($work->imgWorks as $index => $img_work)
                    {
                        $slide_count = 'image-slide-'.($index+1);
                        $slide_src = Yii::app()->getBaseUrl(true).'/shared/uploads/works/'.str_replace('.', 's.', $img_work->name);
                        $slide_not_loaded = '';
                        $slide_current = '';
                        $slide_style = '';

                        if ($index > 0) {
                            $slide_not_loaded = 'img-not-loaded';
                            $slide_style = 'style="z-index:-1;"';
                        } else {
                            $slide_current = 'image-slide-current';
                        }
                ?>
                        <div class="image-slide asd-wrap <?=$slide_count?> <?=$slide_current?> <?=$slide_not_loaded?>" data-src="<?=$slide_src?>" <?=$slide_style?>>
                            <?php if ($index <= 0) { ?>
                                <img class="bg" src="<?=$slide_src?>">
                            <?php } ?>
                        </div>
                <?php } ?>
            <?php } ?>
            <div class="b-work_overlay asd-overlay"></div>
        </div>

        <div class="b-work asd-wrap b-wrap">
            <div class="b-height_works b-height_works_1">
                <div class="b-works_content">
                    <div class="b-content_title b-content_title_works"><span <?=$work_margin?> ><?=$work->title?></span></div>
                    <div class="b-content_descr b-content_descr_works"><?=$work->description?></div>
                    <div class="b-works_buttons">
                        <a class="b-works_button b-works_button_white b-works_button_pc b-works_button_1" href="javascript:void(0)">
                            <div class="b-works_button_in">
                                <span class="b-works_button_top_text">Rollover to see images</span>
                                <span class="b-works_button_bottom_text">Rollout to go back</span>
                            </div>
                        </a>
                        <a class="b-works_button b-works_button_2" href="<?=$work->link?>" target="_blank">
                            <div class="b-works_button_in">
                                <span class="b-works_button_top_text">Launch project</span>
                                <span class="b-works_button_bottom_text">Launch project</span>
                            </div>
                        </a>
                        <a class="b-works_button b-works_button_white b-works_button_mobile b-works_button_3" href="javascript:works_images_slideshow()">
                            <div class="b-works_button_in">
                                <span class="b-works_button_top_text">Images</span>
                                <span class="b-works_button_bottom_text">Go back</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <a href="javascript:set_page('works')" class="b-works_projects_list_btn">Projects List</a>
            <div class="b-works_scroll_next">Scroll to next project</div>
        </div>
    </div>

<?php } ?>