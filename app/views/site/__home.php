<?php
/**
 * @var $home_info Home
 * @var $social_links Social
 */
?>
<div class="b-content_bg asd-wrap">
    <img class="bg" src="<?=Yii::app()->getBaseUrl(true)?>/media/img/bg/home.jpg" />
</div>
<div class="b-content_video asd-wrap" style="display: none;">
    <div class="b-content_video_in asd-wrap" data-src="<?=Yii::app()->getBaseUrl(true)?>/media/video/home"></div>
</div>
<div class="b-content_overlay asd-overlay"></div>

<div class="asd-wrap b-wrap b-home_wrap">
    <?php if (isset($home_info) && count($home_info) > 0) { ?>
        <div class="b-height_main b-height_main_1"></div>
        <div class="b-content_title b-content_title_main"><?=$home_info[0]->title?></div>
        <div class="b-height_main b-height_main_2"></div>
        <div class="b-content_descr b-content_descr_main"><?=$home_info[0]->text?></div>
        <div class="b-height_main b-height_main_3"></div>
    <?php } ?>

    <?php if (isset($social_links)) { ?>
        <div class="b-content_socials b-content_socials_main">
            <div class="b-content_social_title">Become a friend</div>

            <?php foreach ($social_links as $social_link) { ?>
                <div class="b-content_social"><a href="<?=$social_link->link?>" target="_blank"><?=$social_link->name?></a></div>
            <?php } ?>

            <div class="asd-clear"></div>
        </div>
    <?php } ?>
</div>