<?php
/**
 * @var $contacts Contacts
 */
?>
<div class="b-content_bg asd-wrap">
    <img class="bg" src="<?=Yii::app()->getBaseUrl(true)?>/media/img/bg/contacts.jpg" />
</div>
<div class="b-content_video asd-wrap" style="display: none;">
    <div class="b-content_video_in asd-wrap" data-src="<?=Yii::app()->getBaseUrl(true)?>/media/video/contacts"></div>
</div>
<div class="b-content_overlay asd-overlay"></div>

<?php if (isset($contacts) && count($contacts) > 0) { ?>
    <div class="asd-wrap b-wrap b-contacts_wrap">

        <div <?php if (count($contacts) <= 2) { ?> class="b-height_contacts_1" <?php } else { ?> <div class="b-height_contacts_2"> <?php } ?> >
            <div class="b-height_contacts_in">
                <div class="b-contacts_column b-contacts_column_left">
                    <?php foreach ($contacts as $index => $contact) { if ($index < ceil(count($contacts) / 2)) { ?>
                        <div class="b-contact">
                            <div class="b-content_title b-content_title_contacts"><?=$contact->country?></div>
                            <div class="b-content_descr b-content_descr_contacts b-content_descr_contacts_left"><?=$contact->address?></div>
                            <div class="b-content_descr b-content_descr_contacts b-content_descr_contacts_right"><?=$contact->phone?><br/><?=$contact->email?></div>
                            <div class="asd-clear"></div>
                        </div>
                    <?php } } ?>
                </div>
                <div class="b-contacts_column b-contacts_column_right">
                    <?php foreach ($contacts as $index => $contact) { if ($index >= ceil(count($contacts) / 2)) { ?>
                        <div class="b-contact">
                            <div class="b-content_title b-content_title_contacts"><?=$contact->country?></div>
                            <div class="b-content_descr b-content_descr_contacts b-content_descr_contacts_left"><?=$contact->address?></div>
                            <div class="b-content_descr b-content_descr_contacts b-content_descr_contacts_right"><?=$contact->phone?><br/><?=$contact->email?></div>
                            <div class="asd-clear"></div>
                        </div>
                    <?php } } ?>
                </div>
                <div class="asd-clear"></div>
            </div>
        </div>

    </div>
<?php } ?>