<?php
/**
 * @var $works Works
 */
?>
<?php if (isset($works) && count($works) > 0) { ?>
    <div class="asd-wrap b-projects_wrap">
        <div class="b-projects_wrap_in">
            <div class="b-projects_wrap_in2">

                <?php foreach ($works as $index => $work) { ?>
                    <div class="b-project b-project_<?=$work->id?>">
                        <div class="b-project_in asd-wrap">
                            <div class="asd-wrap">
                                <?php if (isset($work->imgWorks) && count($work->imgWorks) > 0) { ?>
                                    <!--<img class="b-project_color_img asd-wrap" src="<?//=Yii::app()->getBaseUrl(true)?>/shared/uploads/works/<?//=str_replace('.', 'c.', $work->imgWorks[0]->name)?>" />-->
                                    <img class="b-project_grey_img asd-wrap" src="<?=Yii::app()->getBaseUrl(true)?>/shared/uploads/works/<?=str_replace('.', 'b.', $work->imgWorks[0]->name)?>" />
                                <?php } ?>
                                <div class="b-project_overlay asd-overlay"></div>
                            </div>
                            <div class="b-project_panel_wrap asd-wrap">
                                <div class="b-project_panel_bg asd-wrap"></div>

                                <a class="b-project_panel asd-wrap" href="javascript:set_work(<?=$work->id?>)">
                                    <div class="b-project_panel_in">
                                        <span class="b-project_panel_text"><?=$work->title?></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="asd-clear"></div>
            </div>
        </div>
    </div>
<?php } ?>
