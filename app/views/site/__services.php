<?php
/**
 * @var $services Services
 */
?>
<div class="b-content_bg asd-wrap">
    <img class="bg" src="<?=Yii::app()->getBaseUrl(true)?>/media/img/bg/services.jpg" />
</div>
<div class="b-content_video asd-wrap" style="display: none;">
    <div class="b-content_video_in asd-wrap" data-src="<?=Yii::app()->getBaseUrl(true)?>/media/video/services"></div>
</div>
<div class="b-content_overlay asd-overlay"></div>

<?php if (isset($services) && count($services) > 0) { ?>
    <div class="asd-wrap b-wrap b-services_slider">

        <div class="b-height_services b-height_services_1">
            <?php foreach ($services as $index => $service) { ?>
                <div class="b-services_slider_content" <?php if ($index > 0) { echo 'style="opacity: 0;"'; } ?> >
                    <div class="b-services_slider_img b-services_slider_img_png">
                        <img src="<?=Yii::app()->getBaseUrl(true)?>/shared/uploads/services/<?=$service->icons_png?>" />
                    </div>
                    <div class="b-services_slider_img b-services_slider_img_svg" style="display: none;">
                        <img src="<?=Yii::app()->getBaseUrl(true)?>/shared/uploads/services/<?=$service->icons_svg?>" />
                    </div>
                    <div class="b-services_slider_text">
                        <div class="b-content_title"><?=$service->title?></div>
                        <div class="b-content_descr b-content_descr_services"><?=$service->text?></div>
                    </div>
                    <div class="asd-clear"></div>
                </div>
            <?php } ?>
        </div>

        <div class="b-services_slider_buttons">
            <?php foreach ($services as $index => $service) { ?>
                <a href="javascript:void(0)" class="b-services_slider_button <?php if ($index == 0) { echo 'b-services_slider_button_active'; } ?>"><span></span></a>
            <?php } ?>
        </div>

    </div>
<?php } ?>