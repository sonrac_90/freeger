<?php
/**
 * Helper для работы с изображениями
 * User: Евгений
 */
/**
 * Class GImage
 * @property string $fileName
 * @property string ext
 */
class GFIleUploader {

    public static $file = null;
    public static $ext = null;
    public static $fileName = null;
    public static $model = null;

    /**
     * @param string $instanceName
     * @return array|CUploadedFile
     */
    public static function uploadImage($instanceName = 'image')
    {
        $model = new GFileModel();
        $model->image = CUploadedFile::getInstanceByName($instanceName);
        self::$ext = $model->image->extensionName;
        self::$fileName = $model->image->name;
        return self::$file = $model->image;
    }

    public static function validate($model = null)
    {
        $model = (isset($model)) ? $model : self::$model;
        if(!$model->validate())
            return $model;
        return true;
    }

    /**
     * @param $path
     * @param null $name
     * @param null $ext
     */
    public static function saveFile($path, $name = null, $ext = null)
    {
        $name = (isset($name)) ? $name : self::$fileName;
        $ext = (isset($ext)) ? $ext : self::$ext;
        return self::$file->saveAs($path . $name . '.' . $ext);
    }

} 