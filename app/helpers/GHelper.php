<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Евгений
 * Date: 08.10.13
 * Time: 20:50
 * To change this template use File | Settings | File Templates.
 */

class GHelper {

    public static function dynamicPath($id)
    {
        $n = (int)$id;

        $a = (int)($n/30000);
        $b = (int)(($n-$a*30000)/300);

        return "{$a}/{$b}";
    }

    //создает рекурсивно путь каталоги по указанному пути
    public static function rmkdir($path, $mode = 0777) {
        return is_dir($path) || (self::rmkdir(dirname($path), $mode) && mkdir($path, $mode) );
    }

    public static function cutString($string, $maxlen = 200, $end = '') {
        $string=stripslashes($string);
        $string=strip_tags($string);
        $len = (mb_strlen($string) > $maxlen)
            ? mb_strripos(mb_substr($string, 0, $maxlen), ' ')
            : $maxlen
        ;
        $cutStr = mb_substr($string, 0, $len);
        return (mb_strlen($string) > $maxlen)
            ?  $cutStr . $end //'...'
            : $cutStr
            ;
    }

    public static function getUser($id = false)
    {
        if($id === false && Yii::app()->user->isGuest)
            return false;
        return Users::model()->findByPk(($id)?$id:Yii::app()->user->id);
    }

    public static function articleBreadcrumb($id, $array = array())
    {
        $article = Pages::model()->findByAttributes(array('id'=>$id,'type'=>Pages::TYPE_ARTICLE));
        if($article)
        {
            if(!empty($array))
                $array[$article->title] = Yii::app()->createUrl('/admin/articles/index',array('id'=>$article->id));
            else
                $array[0] = $article->title;
            if($article->parent_id != NULL)
            {
                return self::articleBreadcrumb($article->parent_id, $array);
            }

        }
        return $array;
    }

    public static function categoriesBreadcrumb($id, $array = array())
    {
        $categories = ShopCategories::model()->findByAttributes(array('id' => $id));
        if($categories)
        {
            if(!empty($array))
                $array[$categories->name] = Yii::app()->createUrl('/admin/gshop/categories/index',array('id'=>$categories->id));
            else
                $array[0] = $categories->name;
            if($categories->parent != NULL)
            {
                return self::categoriesBreadcrumb($categories->parent, $array);
            }

        }
        return $array;
    }



    public static function frontArticleBreadcrumb($id, $array = array())
    {
        $article = Pages::model()->findByAttributes(array('id'=>$id,'type'=>Pages::TYPE_ARTICLE));
        if($article)
        {
            if(!empty($array))
                $array[$article->title] = Yii::app()->createUrl('articles/index',array('alias'=>$article->alias));
            else
                $array[0] = $article->title;
            if($article->parent_id != NULL)
            {
                return self::frontArticleBreadcrumb($article->parent_id, $array);
            }

        }
        return $array;
    }

    /**
     * @param array $params
     * @return array
     */
    public static function jsonSuccess(array $params=array())
    {
        $result = array(
            'status' => 'success'
        );
        echo CJSON::encode(array_merge($params,$result));
        exit;
    }

    /**
     * @param $message
     * @param int $errno
     * @param array $params
     * @return array
     */
    public static function jsonError($message, $errno=1,array $params=array())
    {
        $result = array(
            'status' => 'failure',
            'error' => $errno,
            'message' => $message,
        );
        echo CJSON::encode(array_merge($params,$result));
        exit;
    }

    public static function alias($string)
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => "i",  'ы' => 'y',   'ъ' => "yi",
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => "'",  'Ы' => 'Y',   'Ъ' => "",
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            '-' => '_',   ' ' => '_',  ',' => '_',
            '.' => '',   '!' => '',  '?' => '',
            '/' => '',   '*' => '',  '(' => '',
            ')' => '',   '[' => '',  ']' => '',
            '\\' => '',   '|' => '',  '^' => '',
            ':' => '',   '%' => '',  '$' => '',
            '@' => '',   '#' => '',  '&' => '',

        );
        return strtr($string, $converter);
    }

    /**
     * @param $id
     * @return Pages
     */
    public static function getPage($id)
    {
        return Pages::model()->findByPk($id);
    }

    /**
     * @param $id
     * @return ShopCategories
     */
    public static function getCategory($id)
    {
        return ShopCategories::model()->findByPk($id);
    }

    public static function loadCSS($css = array())
    {
        if(!is_array($css))
        {
            $arr = array();
            $arr[] = $css;
            $css = $arr;
        }
        foreach($css as $css)
        {
            Yii::app()->clientScript->registerCssFile(
                Yii::app()->assetManager->publish(
                    dirname(Yii::getPathOfAlias('application')) . '/media/css/' . $css
                )
            );
        }
    }

    public static function loadJS($js = array(),$pos = null)
    {
        $pos = ($pos) ? $pos : CClientScript::POS_HEAD;
        if(!is_array($js))
        {
            $arr = array();
            $arr[] = $js;
            $js = $arr;
        }
        foreach($js as $js)
        {
            Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                    dirname(Yii::getPathOfAlias('application')) . '/media/js/' . $js
                ),
                $pos
            );
        }
    }

    public static function registerJS($js = array(), $position = null)
    {
        $position = ($position) ? $position : CClientScript::POS_HEAD;
        if(!is_array($js))
        {
            $arr = array();
            $arr[] = $js;
            $js = $arr;
        }
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        foreach($js as $js)
        {
            $cs->registerScriptFile($baseUrl . '/media/js/' . $js, $position);
        }
    }

    public static function registerCSS($js = array(), $position = null)
    {
        $position = ($position) ? $position : CClientScript::POS_HEAD;
        if(!is_array($js))
        {
            $arr = array();
            $arr[] = $js;
            $js = $arr;
        }
        $baseUrl = Yii::app()->baseUrl;

        foreach($js as $js)
        {
            $cs = Yii::app()->getClientScript();
            $cs->registerCssFile($baseUrl . '/media/css/' . $js);
        }
    }

}