<?php
/**
 * Created by PhpStorm.
 * User: Eugene
 * Date: 14.01.14
 * Time: 14:17
 */

class GImage {

    public static $object;
    public static $width;
    public static $height;
    public static $thumbWidth;
    public static $thumbHeight;
    public static $ext = 'jpg';

    /**
     * @param $imagePath
     * @param null $ext
     * @return bool|GFileModel
     */
    public static function openFile($imagePath)
    {
        //self::$ext = (isset($ext)) ? $ext : self::$ext;
        self::$ext = GFileUploader::pathExtension($imagePath);
        switch(strtolower(self::$ext))
        {
            case 'jpg':
            case 'jpeg':
                return self::$object = imagecreatefromjpeg($imagePath);
                break;
            case 'gif':
                return self::$object = imagecreatefromgif($imagePath);
                break;
            case 'png':
                return self::$object = imagecreatefrompng($imagePath);
                break;
        }
        return false;
    }

    public static function cropImage($thumb_width, $thumb_height, $image = null, $black=false)
    {
        $image = ($image) ? $image : self::$object;
        self::$width = imagesx($image);
        self::$height = imagesy($image);
        $original_aspect = self::$width / self::$height;
        $thumb_aspect = $thumb_width / $thumb_height;

        if($original_aspect >= $thumb_aspect)
        {
            $new_height = $thumb_height;
            $new_width = self::$width / (self::$height / $thumb_height);
        }
        else
        {
            $new_width = $thumb_width;
            $new_height = self::$height / (self::$width / $thumb_width);
        }

        if ($black){
            imagefilter($image, IMG_FILTER_GRAYSCALE);
        }

        $thumb = imagecreatetruecolor($thumb_width, $thumb_height);
        imagecopyresampled(
            $thumb,
            $image,
            0 - ($new_width - $thumb_width) / 2,
            0 - ($new_height - $thumb_height) / 2,
            0, 0,
            $new_width, $new_height,
            self::$width, self::$height
        );
        return $thumb;
    }

    public static function imageSave($object, $path, $name)
    {
        $path = str_replace('//','/',$path);
        GHelper::rmkdir($path);
        $ext = GFileUploader::pathExtension($path . $name);
        switch(strtolower($ext))
        {
            case 'jpg':
            case 'jpeg':
                imagejpeg($object, $path . $name, 80);
                return true;
                break;
            case 'gif':
                imagegif($object, $path . $name);
                return true;
                break;
            case 'png':
                imagepng($object, $path . $name, 0);
                return true;
                break;
        }
        return false;
    }

} 