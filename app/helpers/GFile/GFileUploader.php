<?php
/**
 * Class GFileUploader
 * @property string $fileName
 * @property string $ext
 * @property GFileModel $model
 */
class GFileUploader {

    public static $file = null;
    public static $ext = null;
    public static $fileName = null;
    public static $model = null;

    /**
     * @param string $instanceName
     * @return array|CUploadedFile
     */
    public static function uploadImage($instanceName = 'image')
    {
        $model = new GFileModel('image');
        $model->image = CUploadedFile::getInstanceByName($instanceName);
        self::$ext = $model->image->extensionName;
        self::$fileName = $model->image->name;
        self::$file = $model->image;
        return $model;
    }

    public static function validate($model = null)
    {
        $model = (isset($model)) ? $model : self::$model;
        if(!$model->validate())
        {
            return self::$file = $model->errors;
        }
        return true;
    }

    /**
     * @param $path
     * @param null $name
     * @return bool
     */
    public static function saveFile($path, $name = null)
    {
        if(!is_object(self::$file))
            return false;
        GHelper::rmkdir($path);
        $name = (isset($name)) ? $name : self::$fileName;
        return self::$file->saveAs($path . $name);
    }

    public static function pathExtension($file)
    {
        return pathinfo($file, PATHINFO_EXTENSION);
    }

    public static function pathFilename($file)
    {
        return pathinfo($file, PATHINFO_FILENAME);
    }

} 