<?php
class GCropImage {

    /**
     * Get image extension
     * @param $imgPath
     * @return mixed
     */
    public static function getExtension($imgPath){
        return pathinfo($imgPath, PATHINFO_EXTENSION);
    }

    /**
     * Get image source
     * @param $imgPath
     * @return null|resource
     */
    public static function retSource ($imgPath) {
        $imgExt = self::getExtension($imgPath);
        switch ($imgExt){
            case 'jpg'||'jpeg':
                return imagecreatefromjpeg($imgPath);
                break;
            case 'png' :
                return imagecreatefrompng($imgPath);
                break;
            default:
                return null;
        }
    }

    public static function getWidthImage ($imageSource){
        return imagesx ($imageSource);
    }

    public function getHeightImage ($imageSource) {
        return imagesy ($imageSource);
    }

    public static function getSizeImage ($imageSource){
        return array(
            'width' => self::getWidthImage($imageSource),
            'height' => self::getHeightImage($imageSource)
        );
    }

    public static function createImg ($newSize){
        return imagecreatetruecolor($newSize['width'], $newSize['height']);
    }

    public static function cropImageBlack ($image, $newSize){
        imagefilter($image, IMG_FILTER_GRAYSCALE);
        $destination = self::createImg($newSize);
        self::resempledImage($image, $destination, $newSize);
        return $destination;
    }

    public static function resempledImage ($image, &$destination, $newSize){
        $imgSize = self::getSizeImage($image);

        return imagecopyresampled($destination, $image, 0, 0, 0, 0,
            $newSize['width'], $newSize['height'], $imgSize['width'], $imgSize['height']);

    }

    public static function cropImageSize ($image, $scalar){
        $imgSource = self::retSource($image);
        $newSize = array (
            'width' => $scalar * 0.75,
            'height' => $scalar
        );
        $destination = self::createImg($newSize);

        self::resempledImage($imgSource, $destination, $newSize);
        return array (
            'color' => &$destination,
            'black' => self::cropImageBlack($imgSource, $newSize)
        );
    }

    public static function saveImage ($imgSource, $imgPath, $quality=null, $filter=null){
        $extension = self::getExtension($imgPath);

        switch ($extension){
            case ('jpg' || 'jpeg') :
                imagejpeg($imgSource, $imgPath, $quality);
                break;
            case 'png' :
                imagepng($imgSource, $imgPath, $quality, $filter);
                break;
        }
    }

    public static function generatePreview ($image, $scalar) {
        $imageName = explode('.', basename($image));
        $nImg = $imageName[sizeof($imageName) - 1];
        $imageName[sizeof($imageName) - 1] = $nImg. "b";
        $img = array ();
        $img['black'] = implode(".", $imageName);
        $imageName[sizeof($imageName) - 1] = $nImg. "c";
        $img['color'] = implode(".", $imageName);

        $imageSource = self::cropImageSize($image, $scalar);

        foreach ($imageSource as $key=>$imgNext){
            self::saveImage($imgNext, $img[$key]);
        }
    }
}