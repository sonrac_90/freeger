<?php

class ApiController extends Controller
{

    public function actionGet_home()
    {
        $home_info = Home::model()->findAll();
        $social_links = Social::model()->findAll(array('order' => 'position'));

        $this->renderPartial('/site/__home', array(
            'home_info' => $home_info,
            'social_links' => $social_links
        ));
    }

    public function actionGet_services()
    {
        $services = Services::model()->findAll(array('order' => 'position', 'condition' => 'visible = 1'));

        $this->renderPartial('/site/__services', array(
            'services' => $services
        ));
    }

    public function actionGet_awards()
    {
        $this->renderPartial('/site/__awards', array());
    }

    public function actionGet_contacts()
    {
        $contacts = Contacts::model()->findAll(array('order' => 'position'));

        $this->renderPartial('/site/__contacts', array(
            'contacts' => $contacts
        ));
    }

    public function actionGet_works()
    {
        $works = Works::model()->findAll(array('order' => 'id', 'condition' => 'visible = 1'));

        $this->renderPartial('/site/__works', array(
            'works' => $works
        ));
    }

    public function actionGet_work($id = 0)
    {
        if (intval($id) > 0) {
            $work = Works::model()->findByPk(intval($id));
        } else {
            $criteria = new CDbCriteria();
            $criteria->order = ' id DESC ';
            $criteria->condition = 'visible = 1';
            $criteria->limit = 1;
            $work = Works::model()->find($criteria);
        }

        $this->renderPartial('/site/__work', array(
            'work' => $work
        ));
    }

}