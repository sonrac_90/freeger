<?php

class Adminx24Module extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            $this->getName() . '.models.*',
            $this->getName() . '.components.*',
        ));

        Yii::app()->setComponents(array(
                'errorHandler'=>array(
                    'errorAction' => $this->getName() . '/default/error',
                ),
                'user' => array(
                    'allowAutoLogin' => true,
                    'class'          => 'WebUser',
                    'loginUrl'       => Yii::app()->createUrl($this->getName() . '/default/login'),
                ),
            )
        );

        Yii::app()->user->setStateKeyPrefix('_admin');

    }

    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            return true;
        }
        else
            return false;
    }
}
