<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8" />

<?php

$baseUrl = Yii::app()->baseUrl . '/media/';
?>
<link rel="stylesheet" href="<?=$baseUrl?>css/admin/bootstrap/bootstrap.css" />
<link rel="stylesheet" href="<?=$baseUrl?>css/admin/main.css" />
<script>
    var admin_url = "<?=Yii::app()->baseUrl?>/adminx24";
</script>
<script type="text/javascript" src="<?=$baseUrl?>js/admin/jquery.js" ></script>
<script type="text/javascript" src="<?=$baseUrl?>js/admin/jquery-ui.min.js" ></script>
<script type="text/javascript" src="<?=$baseUrl?>js/admin/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?=$baseUrl?>js/admin/main.js" ></script>
</head>
<body>
<div class="navbar navbar-default navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<a href="#" class="navbar-brand"><?/*=Yii::app()->name*/?></a>-->
        </div>
        <div class="collapse navbar-collapse">
    <?php
    $this->widget('zii.widgets.CMenu',
        array(
            'htmlOptions' => array(
                'class' => 'nav navbar-nav',
            ),
            'items'=>array(
                array(
                    'label'=>'Main',
                    'url'=>$this->createUrl('default/index'),
                    'active' => (Yii::app()->controller->id == 'default' && Yii::app()->controller->action->id == 'index'),
                ),
                array(
                    'label'=>'Services',
                    'url'=>$this->createUrl('services/index'),
                    'active' => (Yii::app()->controller->id == 'services'),
                ),
                array(
                    'label'=>'Works',
                    'url'=>$this->createUrl('works/index'),
                    'active' => (Yii::app()->controller->id == 'works'),
                ),
                array(
                    'label'=>'Contacts',
                    'url'=>$this->createUrl('contacts/index'),
                    'active' => (Yii::app()->controller->id == 'contacts'),
                ),
            )
        )
    );
    ?>                    <div class="navbar-right navbar-form">
                <a href="<?= $this->createUrl('default/logout') ?>" class="btn btn-default">Exit(<?=Yii::app()->user->name?>)</a>
            </div>

        </div>
        </div>
</div>
<div class="container">
    <div class="page-header">
        <h1><?=$this->title?></h1>
    </div>
    <?=$content?>

</div>
<?//=var_dump(Yii::app()->baseUrl)?>
</body>
</html>