<?php

echo CHtml::button('Add New', array(
    'onclick' => 'document.location.href=\'' . Yii::app()->baseUrl . '/adminx24/works/addNew' . "'",
    'class' => 'btn btn-success'
));

$this->widget('zii.widgets.grid.CGridView', array(
        'id'               => 'works-list',
        'dataProvider'     => $works->search(),
        'filter'           =>$works,
        'enableHistory'    => true,
        'itemsCssClass'    => 'table table-striped',
        'enablePagination' =>true,
        'template'         => '{items}{pager}',
        'pager'            => array(
            'header'               => '',
            'prevPageLabel'        => '<',
            'nextPageLabel'        => '>',
            'lastPageLabel'        => '>>',
            'firstPageLabel'       => '<<',
            'hiddenPageCssClass'   => '',
            'htmlOptions'          => array('class' => 'pagination pagination-centered'),
            'selectedPageCssClass' => 'active',
        ),
        'pagerCssClass' => 'class',
        'rowCssClassExpression'=>'"items[]_{$data->id}"',
        'columns' => array(
            array(
                'name' => 'id',
                'htmlOptions' => array('class' => 'trId')
            ),
            'title',
            'description',
            'link',
            array(
                'header' => '',
                'type' => 'raw',
                'value' => function ($data){
                        $url = Yii::app()->baseUrl . '/adminx24/works/hide/id/' . $data->id . '/hide/' . intval(!$data->visible);
                        $imageSrc = null;
                        return CHtml::link('', $url, array('class' => 'btn ' . ((!$data->visible)? 'btn-danger': 'btn-success') . ' btn-toggle'));
                    },
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'title' => 'Edit',
                    ),
                    'delete' => array(
                        'title' => 'Delete',
                    ),
                ),
                'htmlOptions' => array('rows' => 40)
            )
        )
    )
);


