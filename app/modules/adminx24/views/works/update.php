<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/media/js/admin/plupload/plupload.full.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/media/js/admin/works.js', CClientScript::POS_HEAD);

$form = $this->beginWidget('CActiveForm', array(
    'id'    => 'service-form',
    'focus' => array($works, 'title')
));

$path = Services::$path;

?>

<?php echo $form->errorSummary($works);?>

<div class="row">
    <input type="hidden" name="ImgWorks[][][works_id]"  id="work_id" value="<?=$works['id']?>" />
    <?=$form->hiddenField($works, 'id')?>
    <?=$form->hiddenField($works, 'position')?>
    <?=$form->hiddenField($works, 'visible')?>
    <?=$form->labelEx($works, 'title')?><br/>
    <?=$form->textField($works, 'title')?>
    <?=$form->error($works, 'title')?>
</div>

<div class="row">
    <?=$form->labelEx($works, 'description')?><br/>
    <?php echo $form->textArea($works, 'description', array('rows' => 10, 'cols' => 100)); ?>
    <?=$form->error($works, 'description')?>
</div>
<div class="row">
    <?=$form->labelEx($works, 'link')?><br/>
    <?php echo $form->textField($works, 'link', array('rows' => 10, 'cols' => 100)); ?>
    <?=$form->error($works, 'link')?>
</div>
<div class="row">
</div>
<div style ="overflow: hidden;" id="imgWorks">
    <?php
    $cnt=-1;
    foreach ($imgWorks as $key => $value) {
        $cnt++;
        if (!$value['name']) continue;
        ?>
        <div class="row left" data="ImgWorks_items[]_<?=$value['id']?>">
            <img src="<?=Yii::app()->baseUrl . Works::$path.$value['name']?>" data="<?=$value['position']?>"/><br/>
            <button class="btn btn-danger" id="imgWorkDelete<?=$value['id']?>">Delete</button>
            <input type="hidden" name="ImgWorks[<?=$cnt?>][name]" value="<?=$value['name']?>" />
            <input type="hidden" name="ImgWorks[<?=$cnt?>][id]" value="<?=($value['id'])?$value['id']:-100?>" />
            <input type="hidden" name="ImgWorks[<?=$cnt?>][works_id]"  id="work_id" value="<?=$value['works_id']?>" />
        </div>
        <?php
    }
    ?>
</div>
    <div id="container" align="center">
        <button id="pickfiles" class="btn btn-success">Upload</button>
    </div>

    <br />
<div class="row" style="display: block;">
<?=CHtml::submitButton('Submit', array('class' => 'btn btn-success'))?>
</div>
<?php $this->endWidget();