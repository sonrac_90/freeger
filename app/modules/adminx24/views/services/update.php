<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/media/js/admin/pekeUpload/pekeUpload.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/media/js/admin/service.js', CClientScript::POS_END);

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'service-form',
    'focus' => array($service, 'title')
));

$path = Services::$path;

?>

<?php echo $form->errorSummary($service);?>

<div class="row">
    <?=$form->hiddenField($service, 'position')?>
    <?=$form->hiddenField($service, 'visible')?>
    <?=$form->labelEx($service, 'title')?><br/>
    <?=$form->textField($service, 'title')?>
    <?=$form->error($service, 'title')?>
</div>

<div class="row">
    <?=$form->labelEx($service, 'text')?><br/>
    <?php echo $form->textArea($service, 'text', array('rows' => 10, 'cols' => 100)); ?>
    <?=$form->error($service, 'text')?>
</div>
<div style ="overflow: hidden;">
<div class="row left">
    <?=$form->labelEx($service, 'icons_png')?><br/>
    <?php echo $form->hiddenField($service, 'icons_png', array('rows' => 10, 'cols' => 100)); ?>
    <?php

    $imageSrc = (is_file(Yii::app()->basePath . '/..' . $path . $service->icons_png)) ?
            $path . $service->icons_png : '/media/img/admin/noImg.png';

    ?>
    <img src="<?=Yii::app()->baseUrl . $imageSrc?>" class="icons" />
    <input type="file" id="png" />
    <?=$form->error($service, 'icons_png')?>
</div>




<div class="row left">
    <?=$form->labelEx($service, 'icons_svg')?><br/>
    <?php echo $form->hiddenField($service, 'icons_svg', array('rows' => 10, 'cols' => 100)); ?>
    <?php

    $imageSrc = (is_file(Yii::app()->basePath . '/..' . $path . $service->icons_svg)) ?
        $path . $service->icons_svg : '/media/img/admin/noImg.png';

    ?>
    <img src="<?=Yii::app()->baseUrl . $imageSrc?>" />
    <input type="file" id="svg" />
    <?=$form->error($service, 'icons_svg')?>

</div>
</div>
<div class="row" style="display: block;">
<?=CHtml::submitButton('Submit', array('class' => 'btn btn-success'))?>
</div>
<?php $this->endWidget();