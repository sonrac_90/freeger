<?php

echo CHtml::button('Add New', array(
    'onclick' => 'document.location.href=\'' . Yii::app()->baseUrl . '/adminx24/services/addNew' . "'",
    'class' => 'btn btn-success'
));

$this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'services-list',
        'dataProvider' => $service->search(),
        'filter'=>$service,
        'enableHistory' => true,
        'itemsCssClass' => 'table table-striped',
        'enablePagination'=>true,
        'template' => '{items}{pager}',
        'pager'         => array(
            'header'         => '',
            'prevPageLabel'  => '<',
            'nextPageLabel'  => '>',
            'lastPageLabel'  => '>>',
            'firstPageLabel' => '<<',
            'hiddenPageCssClass' => '',
            'htmlOptions'    => array(
                'class' => 'pagination pagination-centered',
            ),
            'selectedPageCssClass' => 'active',
        ),
        'pagerCssClass' => '',
        'rowCssClassExpression'=>'"items[]_{$data->id}"',
        'columns' => array(
            array(
                'name' => 'id',
                'htmlOptions' => array('class' => 'trId')
            ),
            'title',
            'text',
            array(
                'name' => 'icons_png',
                'type' => 'html',
                'value' => function ($data){
                        $image_src = Services::$path . $data->icons_png;
                        $image_src = (is_file(Yii::app()->basePath . '/..' . $image_src)) ? $image_src : '/media/img/admin/noImg.png';
                        return '<img src="' . Yii::app()->baseUrl . $image_src . '"/>';},
                'htmlOptions' => array('class' => 'preview'),
                'filter' => false
            ),
            array(
                'name' => 'icons_svg',
                'type' => 'html',
                'value' => function ($data){
                        $image_src = Services::$path . $data->icons_svg;
                        $image_src = (is_file(Yii::app()->basePath . '/..' . $image_src)) ? $image_src : '/media/img/admin/noImg.png';
                        return '<img src="' . Yii::app()->baseUrl . $image_src . '"/>';},
                'htmlOptions' => array('class' => 'preview'),
                'filter' => false
            ),
            array(
                'header' => '',
                'type' => 'raw',
                'value' => function ($data){
                        $url = Yii::app()->baseUrl . '/adminx24/services/hide/id/' . $data->id . '/hide/' . intval(!$data->visible);
                        $imageSrc = null;
                        return CHtml::link('', $url, array('class' => 'btn ' . ((!$data->visible)? 'btn-danger': 'btn-success') . ' btn-toggle'));
                    },
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'title' => 'Edit',
                    ),
                    'delete' => array(
                        'title' => 'Delete',
                    ),
                ),
                'htmlOptions' => array('rows' => 40)
            )
        )
    )
);


