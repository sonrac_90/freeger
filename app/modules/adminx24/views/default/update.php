<?php
/* @var $this DefaultController */
GHelper::registerJS(array('admin/socialImage.js'));

$form = $this->beginWidget('CActiveForm', array(
        'id' => 'socialForm'
    ));
$ext = GFileUploader::pathExtension($model->img);
$fid = GFileUploader::pathFilename($model->img);
$fname =  $fid . '.' . $ext;

$imgLink = Yii::app()->baseUrl . Social::$PATH . "/" . $fname;
echo $form->errorSummary($model);
?>
<div class="social">
    <div class="social">
        <div class="name">
            <div><?php echo $form->label($model, 'name');?></div>
            <div><?php echo $form->textField($model, 'name', array('class' => 'form-control'));?></div>
            <div><?php echo $form->error($model, 'name');?></div>
        </div>
        <div class="link">
            <div><?php echo $form->label($model, 'link');?></div>
            <div><?php echo $form->textField($model, 'link', array('class' => 'form-control'));?></div>
            <div><?php echo $form->error($model, 'link');?></div>
                <div><?php echo $form->hiddenField($model, 'position', array('class' => 'form-control'));?></div>
            <div><?php echo $form->error($model, 'position');?></div>
        </div>
        <?php
        echo CHtml::submitButton('Save', array('class' => 'btn btn-success'));

        $this->endWidget();
    ?>
</div>