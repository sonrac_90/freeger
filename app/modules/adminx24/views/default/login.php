<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
?>
<!doctype html>
<html lang="ru">

<head>
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <meta charset="UTF-8">
    <title><?=Yii::app()->name?> - Вход</title>

    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/media/css/admin/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/media/css/admin/bootstrap/bootstrap-theme.min.css"/>

    <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/media/js/admin/bootstrap.min.js"></script>

    <?php

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        GHelper::loadCSS(
            array(
                'admin/bootstrap/bootstrap.min.css',
                'admin/bootstrap/bootstrap-theme.min.css',
            )
        );
        GHelper::loadJS(
            array(
                'admin/jquery.js',
                'admin/bootstrap.min.js',
            )
        );

    ?>

</head>

    <body>
        <?php

        /**
         * @var $users Users
         * @var $form CActiveForm
         * @var $this AdminController
         */

        $form=$this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        ));
        ?>
        <div class="container" style="margin: 110px auto 5px;">
            <div class="form-group row">
                <div class="span4 offset4 well">
                    <legend align="center">Вход</legend>
                    <?= $form->textField($model, 'username',array('class' => 'form-control', 'placeholder' => 'Логин'))?><br />
                    <?= $form->error($model, 'username'); ?>
                    <?= $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'Пароль'))?><br />
                    <?= $form->error($model, 'password'); ?>

                    <?=CHtml::submitButton('Вход', array('class' => 'btn btn-default btn-block'))?>
                </div>
            </div>
        </div>

        <?php $this->endWidget()?>
    </body>

</html>

