<?php
/* @var $this DefaultController */
GHelper::registerJS(array('admin/socialImage.js'));

$form = $this->beginWidget('CActiveForm', array(
        'id' => 'homeForm'
    ));

echo $form->errorSummary($model);
?>
<div class="social">
    <div class="social">
        <div class="title">
            <div><?php echo $form->labelEx($model, 'title');?></div>
            <div><?php echo $form->textField($model, 'title', array('class' => 'form-control'));?></div>
            <div><?php echo $form->error($model, 'title');?></div>
        </div>
        <div class="text">
            <div><?php echo $form->labelEx($model, 'text');?></div>
            <div><?php echo $form->textArea($model, 'text', array('class' => 'form-control', 'rows' => 10));?></div>
            <div><?php echo $form->error($model, 'text');?></div>
        </div>
        <?php
        echo CHtml::submitButton('Save', array('class' => 'btn btn-success'));

        $this->endWidget();
    ?>
</div>