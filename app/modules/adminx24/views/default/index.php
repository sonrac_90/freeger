<?php

$this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'home-info',
        'dataProvider' => $home->search(),
        'enableHistory' => true,
        'itemsCssClass' => 'table table-striped',
        'rowCssClassExpression'=>'"items[]_{$data->id}"',
        'columns' => array(
            array(
                'name' => 'id',
                'visible' => false
            ),
            array(
                'name' => 'title',
                'htmlOptions' => array(
                    'class' => 'edit-cells',
                    'dataElem' => 'input',
                )
            ),
            array(
                'name' => 'text',
                'htmlOptions' => array(
                    'class' => 'edit-cells',
                    'dataElem' => 'textArea',
                )
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{update}',
                'buttons' => array(
                    'update' => array(
                        'label'=>'Edit',
                        'imageUrl'=>null,
                        'url' => function ($data) {return Yii::app()->baseUrl . '/adminx24/default/updateHome/id/' . $data->id;},
                        'options'=>array('class'=>'glyphicon glyphicon-pencil green'),
                    ),
                )
            )
        )
    )

);

echo CHtml::button('Add Social', array('id' => 'addSocial', 'class' => 'btn btn-success'));

$this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'social-info',
        'dataProvider' => $social->search(),
        'enableHistory' => true,
        'filter' => $social,
        'enableHistory' => true,
        'itemsCssClass' => 'table table-striped',
        'enablePagination'=>true,
        'template' => '{items}{pager}',
        'pager'         => array(
            'header'         => '',
            'prevPageLabel'  => '<',
            'nextPageLabel'  => '>',
            'lastPageLabel'  => '>>',
            'firstPageLabel' => '<<',
            'hiddenPageCssClass' => '',
            'htmlOptions'    => array(
                'class' => 'pagination pagination-centered',
            ),
            'selectedPageCssClass' => 'active',
        ),
        'pagerCssClass' => '',
        'rowCssClassExpression'=>'"items[]_{$data->id}"',
        'columns' => array(
            array(
                'name' => 'id',
                'visible' => false
            ),
            array(
                'name' => 'name',
                'htmlOptions' => array(
                    'class' => 'edit-cells',
                    'dataElem' => 'input',
                )
            ),
            array(
                'name' => 'link',
                'htmlOptions' => array(
                    'class' => 'edit-cells',
                    'dataElem' => 'textArea',
                )
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'label'=>'Edit',
                    ),
                    'delete' => array(
                        'label'=>'Delete',
                    ),
                )
            )
        )
    )

);

?>
<!--<div class="social">-->
<!--    --><?php //foreach ($social as $allSoc) {?>
<!--        <div class="rowSocial" id="num--><?//=$allSoc->id?><!--_--><?//=$allSoc->position?><!--">-->
<!--            <span>--><?//=$allSoc->name?><!--</span>-->
<!--            <span><a href="--><?//=$allSoc->link?><!--" >--><?//=$allSoc->link?><!--</a></span>-->
<!--                <span class="button-column">-->
<!--                    <a class="glyphicon glyphicon-pencil" title="Изменить" href="--><?//=$this->createUrl("update")?><!--/id/--><?//=$allSoc->id?><!--">Изменить</a>-->
<!--                    <a class="glyphicon glyphicon-remove" title="Удалить" href="--><?//=$this->createUrl("delete")?><!--/id/--><?//=$allSoc->id?><!--">Удалить</a>-->
<!--                </span>-->
<!--        </div>-->
<!--    --><?php //}?>
<!--</div>-->
<div id="addNew" ></div>