<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/media/js/admin/pekeUpload/pekeUpload.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/media/js/admin/service.js', CClientScript::POS_END);

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'service-form',
    'focus' => array($contacts, 'title')
));

$path = Services::$path;

?>

<?php echo $form->errorSummary($contacts);?>

<div class="row">
    <?=$form->labelEx($contacts, 'country')?><br/>
    <?=$form->textField($contacts, 'country')?>
    <?=$form->error($contacts, 'country')?>
</div>

<div class="row">
    <?=$form->labelEx($contacts, 'address')?><br/>
    <?php echo $form->textField($contacts, 'address'); ?>
    <?=$form->error($contacts, 'address')?>
</div>
<div class="row">
    <?=$form->labelEx($contacts, 'phone')?><br/>
    <?php echo $form->textField($contacts, 'phone'); ?>
    <?=$form->error($contacts, 'phone')?>
</div>
<div class="row">
    <?=$form->labelEx($contacts, 'email')?><br/>
    <?php echo $form->textField($contacts, 'email', array('rows' => 10, 'cols' => 100)); ?>
    <?=$form->error($contacts, 'email')?>
    <?=$form->hiddenField($contacts, 'position')?>
    <?=$form->hiddenField($contacts, 'id')?>
</div>

<div class="row" style="display: block;">
<?=CHtml::submitButton('Submit', array('class' => 'btn btn-success'))?>
</div>
<?php $this->endWidget();