<?php

echo CHtml::button('Add New', array(
    'onclick' => 'document.location.href=\'' . Yii::app()->baseUrl . '/adminx24/contacts/addNew' . "'",
    'class' => 'btn btn-success'
));

$this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'contacts-list',
        'dataProvider' => $contacts->search(),
        'filter'=>$contacts,
        'enableHistory' => true,
        'itemsCssClass' => 'table table-striped',
        'enablePagination'=>true,
        'template' => '{items}{pager}',
        'pager'         => array(
            'header'         => '',
            'prevPageLabel'  => '<',
            'nextPageLabel'  => '>',
            'lastPageLabel'  => '>>',
            'firstPageLabel' => '<<',
            'hiddenPageCssClass' => '',
            'htmlOptions'    => array(
                'class' => 'pagination pagination-centered',
            ),
            'selectedPageCssClass' => 'active',
        ),
        'pagerCssClass' => '',
        'rowCssClassExpression'=>'"items[]_{$data->id}"',
        'columns' => array(
            array(
                'name' => 'id',
                'htmlOptions' => array(
                    'class' => 'trId',
                )
            ),
            'country',
            'address',
            'phone',
            'email',
            array(
                'class' => 'CButtonColumn',
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'title' => 'Edit',
                    ),
                    'delete' => array(
                        'title' => 'Delete',
                    ),
                ),
            )
        )
    )
);


