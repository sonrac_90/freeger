<?


class ServicesController extends AdminController {

    public $layout = "/layouts/main";
    public $title="Services";

    public function actionIndex(){


        $service = new Services('search');

        $service->unsetAttributes();

        if (isset($_GET['Services'])){
            $service->attributes = $_GET['Services'];
        }

        $this->render('index', array(
            'service' => $service
        ));
    }

    public function actionSort()
    {
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $i = 0;
            foreach ($_POST['items'] as $item) {
                $project = Services::model()->findByPk($item);
                $project->position = $i;
                $project->update();
                $i++;
            }
        }
    }

    public function actionUpdate($id){
        $service = Services::model()->findByPk($id);

        if (isset($_POST['Services'])){
            $service->unsetAttributes();
            $service->attributes = $_POST['Services'];
            $service->id = $id;
            if ($service->update()){
                $this->redirect($this->createUrl('services/index'));
            }
        }

        $this->render('update', array(
                'service' => $service
            )
        );
    }

    public function actionHide($id, $hide){
        $service = Services::model()->findByPk($id);
        if ($service){
            $service->visible = $hide;
            $service->update();
        }
        $this->redirect($this->createUrl('services/index'));
    }

    public function actionDelete($id){
        Services::model()->deleteByPk($id);
        $this->redirect($this->createUrl('services/index'));
    }

    public function actionAddNew(){
        $service = new Services();
        $service->unsetAttributes();

        if (isset($_POST['Services'])){
            $service->attributes = $_POST['Services'];
            if ($service->save()){
                $this->redirect($this->createUrl('services/index'));
            }
        }

        $criteria = new CDbCriteria();
        $criteria->select = "max(position) as max";
        $position = Services::model()->find($criteria);
        $service->position = intval($position['max']) + 1;
        $service->visible = 1;
        $this->render('update', array(
                'service' => $service
            )
        );
    }

    private function moveFile($fileArr, $path){
        $ext = pathinfo($fileArr['name'], PATHINFO_EXTENSION);
        $nameFile = str_replace('.' . $ext, '', $fileArr['name']) . md5(time());
        $fileName = $nameFile . '.' . $ext;
        if (!move_uploaded_file($fileArr['tmp_name'], $path . $fileName)){
            echo CJSON::encode(array(
                'status' => false,
                'message' => "Directory $path not writable"
            ));
            return false;
        }
        return $fileName;
    }

    public function actionUploadImage()
    {
        $path = Yii::app()->basePath . '/..' . Services::$path;
        $newImage = array();
        foreach ($_FILES as $value){
            $newImage  = $this->moveFile($value, $path);
            if (!$newImage) exit();
        }
        echo CJSON::encode(array(
                'status' => true,
                'success' => true,
                'fileName' => $newImage,
                'imageUrl' => Yii::app()->baseUrl . Services::$path .$newImage
            )
        );
    }
}