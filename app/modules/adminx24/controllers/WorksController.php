<?


class WorksController extends AdminController {

    public $layout = "/layouts/main";
    public $title="Works";

    public function actionIndex(){
        $work = new Works('search');

        $work->unsetAttributes();

        if (isset($_GET['Works'])){
            $work->attributes = $_GET['Works'];
        }

        $this->render('index', array(
            'works' => $work
        ));
    }

    public function actionSort()
    {
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $i = 0;
            foreach ($_POST['items'] as $item) {
                $work = Works::model()->findByPk($item);
                $work->position = $i;
                $work->update();
                $i++;
            }
        }
    }

    public function actionSortImageWorks()
    {
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $i = 0;
            foreach ($_POST['items'] as $item) {
                $imgWork = ImgWorks::model()->findByPk($item);
                $imgWork->position = $i;
                $imgWork->update();
                $i++;
            }
        }
    }

    public function actionUpdate($id){
        $work = Works::model()->findByPk($id);
        if (isset($_POST['Works'])){
            $work->unsetAttributes();
            $work->attributes = $_POST['Works'];
            $work->id = $id;
            if ($work->update()){
                $this->redirect($this->createUrl('works/index'));
            }else{
                $work->getErrors();
            }
        }

        $criteria = new CDbCriteria();
        $criteria->order = 'position';
        $criteria->condition = 'works_id = :id';
        $criteria->params = array(':id' => $id);

        $imgWorks = ImgWorks::model()->findAll($criteria);
        $this->render('update', array(
                'works' => $work,
                'imgWorks' => $imgWorks
            )
        );
    }

    public function actionHide($id, $hide){
        $work = Works::model()->findByPk($id);
        if ($work){
            $work->visible = $hide;
            $work->update();
        }
        $this->redirect($this->createUrl('works/index'));
    }

    public function actionDelete($id){
        Works::model()->deleteByPk($id);
        $this->redirect($this->createUrl('works/index'));
    }

    public function actionAddNew()
    {
        $work = new Works();

        if (isset($_POST['Works']))
        {
            $work->unsetAttributes();
            $work->attributes = $_POST['Works'];

            if ($work->save()){
                ImgWorks::model()->updateAll(array('works_id'=>$work->primaryKey), 'works_id = :works_id', array(':works_id' => -100));

                $this->redirect($this->createUrl('works/index'));
            }
        }
        else
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'name';
            $criteria->condition = "works_id = :works_id";
            $criteria->params = array(':works_id' => -100);

            $allFile = ImgWorks::model()->findAll($criteria);


            foreach ($allFile as $value){
                $path = Yii::app()->basePath . '/..' . Works::$path . $value['name'];
                if (is_file($path))
                    unlink($path);
            }


            ImgWorks::model()->deleteAll('works_id = :works_id', array(':works_id' => -100));
        }

        $criteria = new CDbCriteria();
        $criteria->select = 'max(position) as max';
        $position = Works::model()->find($criteria);
        $work->position = intval($position['max'] + 1);


        if (isset($_POST['ImgWorks']) && !isset($imgWorks))
            $imgWorks = $_POST['ImgWorks'];

        $work->visible=1;

        if (!isset($imgWorks))
            $imgWorks = array();

        $this->render('update', array(
                'works' => $work,
                'imgWorks' => $imgWorks
            )
        );
    }

    private function moveFile($fileArr, $path){
        $ext = pathinfo($fileArr['name'], PATHINFO_EXTENSION);
        $nameFile = str_replace('.' . $ext, '', $fileArr['name']) . md5(time());
        $fileName = $nameFile . '.' . $ext;
        if (!move_uploaded_file($fileArr['tmp_name'], $path . $fileName)){
            echo CJSON::encode(array(
                'status' => false,
                'message' => "Directory $path not writable"
            ));
            return false;
        }
        return $fileName;
    }

    public function actionUploadImage()
    {
        $path = Yii::app()->basePath . '/..' . Works::$path;
        $newImage = array();
        foreach ($_FILES as $value){
            $newImage = $this->moveFile($value, $path);
            if (!$newImage) exit();
        }
        echo CJSON::encode(array(
                'status' => true,
                'success' => true,
                'fileName' => $newImage,
                'imageUrl' => Yii::app()->baseUrl . Works::$path .$newImage
            )
        );
    }

    public function actionDeleteImgWorks($id){
        ImgWorks::model()->deleteByPk($id);
    }

    public function actionUploadImages($id){
        $file = GFileUploader::uploadImage('file');
        if(GFileUploader::validate($file))
        {
            $ext = strtolower(GFileUploader::$ext);

            $model = new ImgWorks();

            $criteria = new CDbCriteria();
            $criteria->select = 'max(position) as max';
            $criteria->condition = "works_id = :works_id";
            $criteria->params = array(':works_id' => $id);

            $position = ImgWorks::model()->find($criteria);

            $model->position = intval($position['max'] + 1);

            $criteria = new CDbCriteria();
            $criteria->select = 'max(id) as max';
            $position = ImgWorks::model()->find($criteria);

            $model->works_id = $id;
            $filename = intval($position['max'] + 1) . '.' . $ext;

            $model->name = $filename;

            if($model->save())
            {
                $basePath = dirname(Yii::app()->basePath);
                if(GFileUploader::saveFile($basePath . Works::$path, $filename))
                {
                    $image = GImage::openFile($basePath . Works::$path . $filename);

                    GImage::imageSave($image, $basePath . Works::$path, $filename);

                    $width = 350;
                    $height = $width * 0.75;
                    $colorImage = GImage::cropImage($width, $height, $image);
                    $blackImage = GImage::cropImage($width, $height, $image, true);
                    $blackImage500 = GImage::cropImage(800, 800 * 0.75, $image, true);
                    $newPath = explode(".", $filename);
                    $name = $newPath[sizeof($newPath) - 2];
                    $newPath[sizeof($newPath) - 2] = $name. "c";
                    $newColorPath = implode(".", $newPath);
                    $newPath[sizeof($newPath) - 2] = $name. "b";
                    $newBlackPath = implode(".", $newPath);
                    $newPath[sizeof($newPath) - 2] = $name. "s";
                    $newBlackPath500 = implode(".", $newPath);

                    GImage::imageSave($colorImage, $basePath . Works::$path, $newColorPath);
                    GImage::imageSave($blackImage, $basePath . Works::$path, $newBlackPath);
                    GImage::imageSave($blackImage500, $basePath . Works::$path, $newBlackPath500);
                    GHelper::jsonSuccess(
                        array(
                            'status' => 'success',
                            'image_url' => Yii::app()->baseUrl . Works::$path . $filename,
                            'image_name' => $filename,
                            'id' => $model->primaryKey,
                        )
                    );
                }
                $model->delete();
            }else{
                print_r($model->getErrors());
            }
        }
        GHelper::jsonError('Ошибка сохранения файла');
    }
}