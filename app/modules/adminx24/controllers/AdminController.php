<?php
class AdminController extends CController {

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', //Вход, Выход и страницу ошибок показываем всем, включая гостя
                'actions'=>array('login', 'logout', 'error'),
                'users'=>array('*'),
            ),
            array('allow', // Разрешим всё главному админу
                'roles' => array('admin'),
            ),

            array('deny',  // всё остальное всем запрещаем
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Авторизация в админку
     */
    public function actionLogin()
    {
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            if($model->validate() && $model->login())
            {

                if(str_replace('/', '', Yii::app()->request->baseUrl) == str_replace('/', '', Yii::app()->user->returnUrl))
                {
                    $this->redirect($this->createUrl('default/index'));
                }
                else
                {
                    $this->redirect(Yii::app()->user->returnUrl);
                }

            }
        }
        // display the login form
        $this->renderPartial('login',array('model'=>$model));
    }

    /**
     * Выход из админки
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect($this->createUrl('default/login'));
    }

    /**
     * Страница обработки ошибок
     */
    public function actionError(){
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else{
                $this->render($this->createUrl('default/index'));
            }
        }
    }

}