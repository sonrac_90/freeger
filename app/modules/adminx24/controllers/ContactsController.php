<?


class ContactsController extends AdminController {

    public $layout = "/layouts/main";
    public $title="Contacts";

    public function actionIndex(){


        $contacts = new Contacts('search');

        $contacts->unsetAttributes();

        if (isset($_GET['Contacts'])){
            $contacts->attributes = $_GET['Contacts'];
        }

        if (isset($_POST['Contacts'])){
            $contacts->attributes = $_POST['Contacts'];
            if (!$contacts->save()){
                $this->render('update', array(
                    'contacts' => $contacts
                ));
            }
        }


        $this->render('index', array(
            'contacts' => $contacts
        ));
    }

    public function actionSort()
    {
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $i = 0;
            foreach ($_POST['items'] as $item) {
                $contact = Contacts::model()->findByPk($item);
                $contact->position = $i;
                $contact->update();
                $i++;
            }
        }
    }

    public function actionUpdate($id){
        $contact = Contacts::model()->findByPk($id);

        if (isset($_POST['Contacts'])){
            $contact->unsetAttributes();
            $contact->attributes = $_POST['Contacts'];
            $contact->id = $id;
            if ($contact->update()){
                $this->redirect($this->createUrl('contacts/index'));
            }
        }

        $this->render('update', array(
                'contacts' => $contact
            )
        );
    }

    public function actionDelete($id){
        Contacts::model()->deleteByPk($id);
        $this->redirect($this->createUrl('contacts/index'));
    }

    public function actionAddNew(){
        $contact = new Contacts();
        $criteria = new CDbCriteria();
        $criteria->select = "max(position) as max";
        $position = Contacts::model()->find($criteria);

        if (isset($_POST['Contacts'])){
            $contact->unsetAttributes();
            $criteria = new CDbCriteria();
            $criteria->select = 'max(position) as max';
            $position = Works::model()->find($criteria);
            $contact->attributes = $_POST['Contacts'];
            $contact->position = $position['max'];
            if ($contact->save()){
                unset($_POST);
                $contact = new Contacts('search');
                $this->render('index', array(
                    'contacts' => $contact
                ));
                return;
            }
        }

        $contact->position = intval($position['max']) + 1;
        $this->render('update', array(
                'contacts' => $contact
            )
        );
    }
}