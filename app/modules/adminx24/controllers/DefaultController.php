<?php

class DefaultController extends AdminController
{
    public $layout = "/layouts/main";

    public $title="Main";

	public function actionIndex(){
        $home = new Home('search');
        if (isset($_POST['Home'])){
            $home->unsetAttributes();
            $home->updateAll($_POST['Home']);
        }

        $social = new Social('search');

		$this->render('index', array(
                'home' => $home,
                'social' => $social
            )
        );
	}


    public function actionLoadImage()
    {
        $file = GFileUploader::uploadImage('file');
        if(GFileUploader::validate($file))
        {
            $ext = strtolower(GFileUploader::$ext);
            $rand = time() . rand(0 , 10);
            $dp = GHelper::dynamicPath($rand);
            $basePath = dirname(Yii::app()->basePath);
            $filename = $rand . '.' . $ext;
            if(GFileUploader::saveFile($basePath . Social::$PATH . '/', $filename))
            {
                GHelper::jsonSuccess(
                    array(
                        'image_url' => Yii::app()->baseUrl . Social::$PATH . '/' . $filename,
                        'imageName' => $filename
                    )
                );
            }
        }
        GHelper::jsonError('Ошибка сохранения файла');
    }

    public function actionAdd(){
        $model = new Social();
        if (Yii::app()->request->isPostRequest){
            $model->attributes = $_POST['Social'];
            if ($model->save()){
                echo json_encode($model->attributes);
            }else {

                echo json_encode(array('success' => false, 'err' => $model->getErrors()));
            }
        }
    }

    public function actionUpdate($id){
        $model = Social::model()->findByPk($id);
        if (Yii::app()->request->isPostRequest){
            $model->attributes = $_POST['Social'];
            if ($model->update()){
                $this->redirect($this->createUrl('index'));
            }
        }

        $this->render('update', array('model' => $model));
    }

    public function actionUpdateHome($id){
        $model = Home::model()->findByPk($id);
        if (Yii::app()->request->isPostRequest){
            $model->attributes = $_POST['Home'];
            if ($model->update()){
                $this->redirect($this->createUrl('index'));
            }
        }

        $this->render('updateHome', array('model' => $model));
    }

    public function actionDelete($id){
        $model = new Social();
        $model->deleteByPk($id);
        $this->redirect($this->createUrl("index"));
    }

    public function actionChangeIndex(){
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $i = 0;
            foreach ($_POST['items'] as $item) {
                $work = Social::model()->findByPk($item);
                $work->position = $i;
                $work->update();
                $i++;
            }
        }
    }
}