/************************************************* RESIZERS ******************************************************/

function bg_img_onload(event, percents) {
    percents = (typeof percents === 'undefined') ? false : percents;

    $('img.bg').load(function(){
        $(this).resizer({ percents : percents });
    });
}

function bg_img_update(event, percents) {
    percents = (typeof percents === 'undefined') ? false : percents;

    $('.bg').resizer({ percents : percents });

    works_fontautosize();
}

function content_onresize() {
    var window_height = $('body').height();
    var header_height = $('.b-header_height').height();

    $('.b-content').css({
        'height' : window_height - header_height + 1
    });

    if ($('.b-menu_button').is(':visible')) {
        main_menu_autosize();
    } else {
        main_menu_hide();
    }
}

function works_fontautosize() {
    if (NOW == 'work') {
        $('#wrap-work_id'+WORK_ID+' .b-content_title_works span').fontautosize();
    }
}

function projects_onresize() {
    if ($('.b-project').length > 0) {
        var resolutions = [
            {'width' : 1700, 'count' : 6},
            {'width' : 1400, 'count' : 5},
            {'width' : 1000, 'count' : 4},
            {'width' : 700, 'count' : 3},
            {'width' : 450, 'count' : 2},
            {'width' : 0, 'count' : 1}
        ];

        var window_width = $('body').width();
        var project_margin_left = parseInt($('.b-project').css('margin-left'));
        var project_margin_right = parseInt($('.b-project').css('margin-right'));
        var project_count = $('.b-project').length;

        var count_projects_in_line = 0;
        for (var i = 0; i < resolutions.length; i++) {
            if (window_width >= resolutions[i].width) {
                count_projects_in_line = resolutions[i].count;
                break;
            }
        }

        var project_width = Math.ceil((window_width - count_projects_in_line * (project_margin_left + project_margin_right)) / count_projects_in_line);
        var project_height = Math.ceil(project_width * 0.75);

        // скрываем последние проекты, чтобы не было пустого места
        $('.b-project').css({ 'width' : project_width, 'height' : project_height }).show();
        if ((project_count % count_projects_in_line) > 0) {
            for (var i = 1; i <= (project_count % count_projects_in_line); i++) {
                $('.b-project').eq(-i).hide();
            }
        }

        var project_panel_width = project_width - parseInt($('.b-project_panel').css('border-left-width')) - parseInt($('.b-project_panel').css('border-right-width'));
        var project_panel_height = project_height - parseInt($('.b-project_panel').css('border-top-width')) - parseInt($('.b-project_panel').css('border-bottom-width'));

        $('.b-project_panel').css({ 'width' : project_panel_width, 'height' : project_panel_height });
    }
}