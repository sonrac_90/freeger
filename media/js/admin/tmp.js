
var sortable = function (param){
    this.selector = '.sort-list';
    this.callback = function(response){};
    this.action = '';
    this.attrSort = '';
    this.ajaxOptions = {
        type: 'POST',
        dataType: 'JSON',
        url: '',
        success : function (response, obj){

        },
        error : function (xhr, err, obj){
            console.log(xhr,  err);
        }
    };

    function checkParam (param){
        if (typeof param === 'undefined') return ;
        for (var i in param){
            if (typeof (this[i]) !== 'undefined'){
                if ( i == 'ajaxOptions'){
                    for (var j in param[i]) {
                        if (typeof(this[i][j]) !== 'undefined'){
                            this[i][j] = param[i][j];
                        }
                    }
                }else this[i] = param[i];
            }
        }
    }

    checkParam(param);

    $(selector).sortable();

    _self = this;

    $(this.selector).on('dragStop', function (event){
        if (typeof(_self.callback) !== 'undefined'){
            _self.callback($(_self.selector).toArray({'attribute' : _self.attrSort}));
            return ;
        }

        $.ajax({
            method : _self.ajaxOptions.type,
            url: _self.ajaxOptions.url,
            dataType: _self.ajaxOptions.dataType,
            success : function (data){
                _self.ajaxOptions.success(data, _self);
            },
            error : function (){
                _self.ajaxOptions.error(xhr, err, _self);
            }
        });
    });
}


var editButton = function (param){
    this.gridId = '';
    this.selector = '.edit';
    this.selectEdit = '[class^=edit-cells]';
    this.specificTable = {
        gridView : '$.fn.yii.grid.update'
    }
    this.ajaxOptions = {
        type: 'POST',
        dataType: 'JSON',
        url: '',
        success : function (id, response, obj){
            var parentElem = _selfEdit.editDataElem(obj);
            var sel = $(_selfEdit.selectEdit, parentElem);
            var cnt = 0;
            for (var i in response){
                if (typeof (response[i]) == 'string'){
                    $(sel[cnt]).html(response[i]);
                    cnt++;
                }
            }
        },
        error : function (xhr, err, obj){}
    };

    this.editDataElem = function (elem) {
        var element = ($(elem).parent().find(_selfEdit.selectEdit).size() > 0)
            ? $(elem).parent()
            : $(elem).parent().parent();
        console.log(element);
        return element;
    }
    _selfEdit = this;
    _selfEdit.checkParam = function (param){
        if (typeof param === 'undefined') return ;
        for (var i in param){
            if (typeof (_selfEdit[i]) !== 'undefined'){
                if ( i == 'ajaxOptions'){
                    for (var j in param[i]) {
                        if (typeof(_selfEdit[i][j]) !== 'undefined'){
                            _selfEdit[i][j] = param[i][j];
                        }
                    }
                }else _selfEdit[i] = param[i];
            }
            return _selfEdit;
        }
    }

    _selfEdit = _selfEdit.checkParam(param);

    _selfEdit.getAction = function (){
        var attrVal = 'editCall' + $(this.tagName + '[data^=editCall]').size();
        $(this).attr('data', attrVal);
        var success = _selfEdit.ajaxOptions.success;
        if ($(document.body).find('grid-view').find('[data^=' + attrVal + ']').size() > 0)
            success = _selfEdit.specificTable.gridView;
        return success;
    }


    _selfEdit.setEvent = function (edit){
        $(edit.selector).each(function (){
            if (this.href) this.href = '#';
            $(this).click(function (){
                console.log(1);
                var parentElem = _selfEdit.editDataElem(this);

                var success = _selfEdit.getAction();

                // CreateInput
                $(edit.selectEdit, parentElem).each(function (){
                    var tag = $(this).attr('dataElem');
                    if (tag.toLowerCase() == 'textarea'){
                        var input = $('<' + tag + '/>', {
                            value : $(this).text(),
                            name: $(this).attr('name'),
                            class: this.className,
                            style:"width:100%; height: 450px"
                        })
                    }else{
                        var input = $('<' + tag + '/>', {
                            value : $(this).text(),
                            name: $(this).attr('name'),
                            class: this.className
                        });
                    }

                    $(this).empty();
                    $(this).append(input);
                });
                var btnSucc = $('<button/>', {class : 'btn btn-success'}).on('click', function (){
                    $(this).remove();
                    $.ajax({
                        method : edit.ajaxOptions.type,
                        url: edit.ajaxOptions.url,
//                        dataType: edit.ajaxOptions.dataType,
                        success : function (data){
                            console.log(11);
                            edit.ajaxOptions.success(edit.gridId, data, edit);
                        },
                        error : function (xhr, err){
                            console.log(22)
                            edit.ajaxOptions.error(xhr, err, edit);
                        }
                    });
                }).html('Save');
                $(this).parent().append(btnSucc);
                $(this).removeAttr('data');
                return false;
            })
        });
    }
    return _selfEdit;

}
