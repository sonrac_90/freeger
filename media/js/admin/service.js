$(document).ready(function (){
    $('#png, #svg').each(function (){
        var type = this.id;
        $(this).pekeUpload({
            btnText: 'Choose',
            url : admin_url + '/services/uploadImage/',
            theme:'bootstrap',
            field:'UploadImg',
            multi: true,
            showFilename:false,
            showPercent:false,
            showErrorAlerts:true,
            maxSize:10,
            allowedExtensions: type,
            onFileError:function (file, error){
                alert(error);
            },
            onFileSuccess: function (file, data){
                $('.pekecontainer').remove();
                var parent = $('#' + this.allowedExtensions).parent();
                $('img', parent).attr('src', data.imageUrl);
                $('input[type^=hidden]', parent).val(data.fileName);
            }
        });
    });
})

function basename(path) {
    return path.replace(/\\/g,'/').replace( /.*\//, '' );
}

function dirname(path) {
    return path.replace(/\\/g,'/').replace(/\/[^\/]*$/, '');;
}