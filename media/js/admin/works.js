$(document).ready(function (){

    eventDelete();

    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',

        browse_button : 'pickfiles', // you can pass in id...
        container: document.getElementById('container'), // ... or DOM Element itself

        url : admin_url + "/works/uploadImages/id/" + ($($('#work_id')[0]).val() ? $($('#work_id')[0]).val() : -100),

        filters : {
            max_file_size : '10mb',
            mime_types: [
                {
                    title : "Image files",
                    extensions : "jpg,gif,png,jpeg"
                }
            ]
        },

        // Flash settings
        flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',

        // Silverlight settings
        silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',


        init: {

            PostInit: function() {

            },

            FilesAdded: function(up, files) {
                uploader.start();
            },

            FileUploaded: function(up, file, info) {

                var info = JSON.parse(info.response);
                var htmlImg = htmlNewImage(info.image_url, info.image_name, info.id, ($($('#work_id')[0]).val() ? $($('#work_id')[0]).val() : -100));
                $('#imgWorks').append(htmlImg);
                eventDelete();
//                var $json = JSON.parse(info.response);
//                var $imageDiv = $('<div>');
//                var $image = $('<img>');
//                var $input = $('<input>');
//                if($json.status == 'success')
//                {
//                    $imageDiv.append($image).append($input);
//                    $imageDiv.addClass('prev_image');
//                    $image.attr('src', $json.image_url);
//                    $image.attr('width', '109px');
//                    $image.attr('height', '77px');
//                    $input.attr('type','hidden');
//                    $input.attr('name','project_prev_image');
//                    $input.attr('value',$json.original);
//
//                    $('.project_image .image_preview').html($imageDiv);
//                }
            }
        }
    });

    uploader.init();

});

function eventDelete(){
    $('[id^=imgWorkDelete]').click(function (event){
        event.preventDefault();
        var self = this;
        var id = this.id.replace(/imgWorkDelete/g, '');
        if (typeof id == 'undefined')
            id = -100;
        $.ajax({
            method: 'POST',
            url: admin_url + '/works/deleteImgWorks/id/' + id,
            success: function (data){
                $(self).parent().remove();
            },
            error:function (xhr, err){
            }
        })
        return false;
    })
}

function htmlNewImage(imgSrc, imgName, id, works_id){
    return '<div class="row left" data="ImgWorks_items[]_' + id + ']">' +
        '<img src="' + imgSrc + '" /><br/>' +
        '<button class="btn btn-danger" id="imgWorkDelete' + id + '">Delete</button>' +
        '<input type="hidden" name="ImgWorks[' + id + '][name]" value="' + imgName + '" />' +
        '<input type="hidden" name="ImgWorks[' + id + '][id]" value="' + id + '" />' +
        '<input type="hidden" name="ImgWorks[' + id + '][works_id]"  id="work_id" value="' + works_id + '" />' +
            '</div>';
}