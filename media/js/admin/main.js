$(document).ready(function (){

    $('a.delete').each(function (){
        $(this).click(function (event){
            event.preventDefault();
            var conf = confirm('Delete it?');

            if (conf) document.location.href = this.href;

            return false;
        })
    });
    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    if ($('#services-list table.table tbody').size())
        $('#services-list table.table tbody').sortable({
            forcePlaceholderSize: true,
            forceHelperSize: true,
            items: 'tr',
            update : function () {
                serial = $('#services-list table.table tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                $.ajax({
                    'url': admin_url + '/services/sort',
                    'type': 'post',
                    'data': serial,
                    'success': function(data){
                    },
                    'error': function(request, status, error){
                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });
            },
            helper: fixHelper
        }).disableSelection();


    if ($('#contacts-list table.table tbody').size())
        $('#contacts-list table.table tbody').sortable({
            forcePlaceholderSize: true,
            forceHelperSize: true,
            items: 'tr',
            update : function () {
                serial = $('#contacts-list table.table tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                $.ajax({
                    'url': admin_url + '/contacts/sort',
                    'type': 'post',
                    'data': serial,
                    'success': function(data){
                    },
                    'error': function(request, status, error){
                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });
            },
            helper: fixHelper
        }).disableSelection();

    if ($('#works-list table.table tbody').size())
        $('#works-list table.table tbody').sortable({
            forcePlaceholderSize: true,
            forceHelperSize: true,
            items: 'tr',
            update : function () {
                serial = $('#works-list table.table tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                $.ajax({
                    'url': admin_url + '/works/sort',
                    'type': 'post',
                    'data': serial,
                    'success': function(data){
                    },
                    'error': function(request, status, error){
                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });
            },
            helper: fixHelper
        }).disableSelection();

    if ($('#social-info table.table tbody').size()){

        $('#social-info table.table tbody').sortable({
            forcePlaceholderSize: true,
            forceHelperSize: true,
            items: 'tr',
            update : function () {
                serial = $('#social-info table.table tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                $.ajax({
                    'url': admin_url + '/default/changeIndex',
                    'type': 'post',
                    'data': serial,
                    'success': function(data){
                    },
                    'error': function(request, status, error){
                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });
            },
            helper: fixHelper
        }).disableSelection();
    }

    if ($('#imgWorks'))
        $('#imgWorks').sortable({
            forcePlaceholderSize: true,
            forceHelperSize: true,
            items: 'div',
            update : function () {
                serial = $('#imgWorks').sortable('serialize', {key: 'items[]', attribute: 'data'});
                $.ajax({
                    'url': admin_url + '/works/sortImageWorks',
                    'type': 'post',
                    'data': serial,
                    'success': function(data){
                    },
                    'error': function(request, status, error){
                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });
            },
            helper: fixHelper
        }).disableSelection();


    $('#fileAbout').on('change', function (){
        var form = document.querySelector('form[id^="contactsForm"]');
        var formData = new FormData(form);

        $.ajax({
            type: 'POST',
            url: admin_url + "/default/loadimage",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(data) {
                $(".image img").attr('src', data.image_url);
                $('form #contactsImageLink').val(data.imageName);
            },
            error: function(data) {
            }
        });

    });

    var successIMgLoad = function (data){
        $(".image img").attr('src', data.image_url);
        $('form #socialImageLink').val(data.imageName);
    }

    var successAdd = function (data){
        $('#addNew #error').remove();
        if (typeof data.success !== 'undefined'){
            var div = $('<div></div>', {'id' : 'error'}).html('Заполните все поля!!!');
            $('#addNew').prepend(div);
            return false;
        }

        var divNewElem = $('<div/>', {className: "", id: 'num' + data.id + '_' + data.position}).addClass('rowSocial')
            .append($('<span/>').html(data.name))
            .append($('<span/>').append($('<img/>', {src: admin_url + '/../uploads/images/content/' + data.img, width: 20, height: 20})))
            .append($('<span/>').append($('<a/>', {href: data.link}).html(data.link)))
            .append($('<span/>').addClass('button-column')
                .append($('<a/>', {href: admin_url + '/default/update/id/' + data.id}).html('Изменить').addClass('glyphicon glyphicon-pencil'))
                .append($('<a/>', {href: admin_url + '/default/delete/id/' + data.id}).html('Удалить').addClass('glyphicon glyphicon-pencil'))
            )

        $(".social").append(divNewElem);
        $('#addNew').html('');
        return false;
    }

    var sendData = function (event){
        var form = document.querySelector('form[id^="socialForm"]');
        var formData = new FormData(form);

        var url = admin_url + '/default/add';
        var callback = successAdd;
        if (this.tagName.toLowerCase() == 'input'){
            url = admin_url + "/default/loadimage";
            callback = successIMgLoad;
        }
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: callback,
            error: function(data) {
            }
        });
        return false;
    };

    var send = function (){
        $('#addNew').html('');
    }

    $('#addSocial').on('click', function (){
        var html = '<div class="content"><form id="socialForm" action="/adminx24/default/add" method="post"><div class="social"><div class="social"><div class="name"><div><label for="ContactSocial_name">Social Network</label></div><div><input class="form-control" name="Social[name]" id="ContactSocial_name" type="text" maxlength="255"></div><div></div>';
        html+='</div>';
        html +='</div><div class="link"><div><label for="ContactSocial_link">Link</label></div><div><input class="form-control" name="Social[link]" id="ContactSocial_link" type="text" maxlength="255"></div><div></div>';
        html += '<div><input class="form-control" name="Social[position]" id="ContactSocial_position" type="hidden" value="4"></div><div></div></div><input class="btn btn-success" type="submit" name="yt0" value="Save"></div>            </div></form></div>';
        $('#addNew').html(html);

        $('#fileAbout').on('change', sendData);
        $('#addNew form').on('submit', sendData);

    });
});