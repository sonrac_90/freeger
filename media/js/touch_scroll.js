/*********************************************** TOUCH SCROLL *****************************************************/

var tc_item = 0;
var tc_drag = false;
var tc_move = false;

var tc_draggable = { 'now' : '', 'work_id' : '' };

var tc_start_top = 0;
var tc_scroll_delta = 0;

function touch_scroll_init()
{
    tc_item = $('.b-content').eq(0);
    touch_scroll_reset();

    if (isTouchSupported)
    {
        tc_item.get(0).addEventListener('touchstart', function(e) {
            if (!tc_drag) {
                tc_drag = true;

                var touch = e.changedTouches[0];
                touch_scroll_start(touch.clientX, touch.clientY, e);
            }
        });

        tc_item.get(0).addEventListener('touchmove', function(e) {
            if (tc_drag) {
                if (NOW !== 'works') {
                    e.preventDefault();
                }

                var touch = e.changedTouches[0];
                if (touch_scroll_move(touch.clientX, touch.clientY, e)) {
                    tc_move = true;
                }
            }
        });

        tc_item.get(0).addEventListener('touchend', function(e) {
            if (tc_drag) {
                touch_scroll_end();
                touch_scroll_reset();
            }
        });
    }
}

function touch_scroll_start(X, Y, e)
{
    tc_start_top = Y;
}

function touch_scroll_move(X, Y, e)
{
    // delta
    tc_scroll_delta = Y - tc_start_top;

    // only at first time
    if (!tc_move) {
        // prev and next page
        var pages_prevnext = pages_get_prevnext();
        if (tc_scroll_delta > 0) {
            tc_draggable = pages_prevnext.prev;
        } else {
            tc_draggable = pages_prevnext.next;
        }

        // reset
        if (tc_draggable.now === '' || (NOW === 'work' && $('#wrap-work_id'+WORK_ID+' .b-work_wrap').hasClass('b-work_slideshow'))) {
            touch_scroll_reset();
            return false;
        }
        if (NOW === 'services') {
            var current_slide = parseInt($('.b-services_slider_button_active').attr('data-group_num'));
            var count_slides = $('.b-services_slider_button').length;

            if (1 < current_slide && current_slide < count_slides || current_slide == 1 && tc_scroll_delta < 0 || current_slide == count_slides && tc_scroll_delta > 0) {
                touch_scroll_reset();
                return false;
            }
        }
    }

    // prevent default
    if (NOW !== 'works') {
        e.preventDefault();
    } else {
        var projects_wrap_height = $('.b-projects_wrap_in').height();
        var projects_wrap_full_height = $('.b-projects_wrap_in2').height();
        var projects_scroll_top = $('.b-projects_wrap_in').scrollTop();

        if (((projects_wrap_full_height - projects_wrap_height) <= projects_scroll_top) && (tc_scroll_delta < 0) || projects_scroll_top <= 0 && (tc_scroll_delta > 0)) {
            e.preventDefault();
        } else {
            touch_scroll_reset();
            return false;
        }
    }

    // work id css
    var work_id_css = '';
    if (tc_draggable.now === 'work') {
        work_id_css = '_id'+tc_draggable.work_id;
    }

    // start position
    var body_height = $('body').height();
    var scroll_percents = (tc_scroll_delta / body_height) * 100;
    if (!tc_move) {
        if (scroll_percents < 0) {
            $('#wrap-' + tc_draggable.now + work_id_css).stop(true).css({ 'top' : '100%' }).attr('data-top', '100');
        } else if (scroll_percents > 0) {
            $('#wrap-' + tc_draggable.now + work_id_css).stop(true).css({ 'top' : '-100%' }).attr('data-top', '-100');
        }
    }

    // new position
    var start_percents = parseInt($('#wrap-' + tc_draggable.now + work_id_css).attr('data-top'));
    var new_percents = start_percents + scroll_percents;
    if (start_percents == 100) {
        new_percents = Math.min(100, Math.max(0, new_percents));
    } else if (start_percents == -100) {
        new_percents = Math.max(-100, Math.min(0, new_percents));
    }

    $('#wrap-' + tc_draggable.now + work_id_css).stop(true).css({ 'top' : new_percents+'%', 'z-index' : 10 });

    return true;
}

function touch_scroll_end()
{
    var body_height = $('body').height();
    var scroll_percents = (tc_scroll_delta / body_height) * 100;

    // scroll ok
    if (Math.abs(scroll_percents) > 30 && tc_draggable.now !== '') {
        $('#wrap-' + tc_draggable.now + work_id_css).stop(true).animate({ 'top' : 0 }, 300, function(){
            $('#wrap-' + tc_draggable.now + work_id_css).css({ 'z-index' : '' });
        });

        // по другому не будет работать =\
        if (tc_draggable.now == 'work') {
            set_work(tc_draggable.work_id, 'touch');
        } else {
            set_page(tc_draggable.now, 'touch');
        }
    }
    // scroll cancel
    else {
        var work_id_css = '';
        if (tc_draggable.now === 'work') {
            work_id_css = '_id'+tc_draggable.work_id;
        }

        $('#wrap-' + tc_draggable.now + work_id_css).stop(true).animate({ 'top' : $('#wrap-' + tc_draggable.now + work_id_css).attr('data-top')+'%' }, 300, function(){
            $('#wrap-' + tc_draggable.now + work_id_css).css({ 'z-index' : '' });
        });
    }
}

function touch_scroll_reset() {
    tc_drag = false;
    tc_move = false;

    tc_draggable = { 'now' : '', 'work_id' : '' };

    tc_start_top = 0;
    tc_scroll_delta = 0;
}


























