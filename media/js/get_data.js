/********************************************** GET ALL DATA ***********************************************/

/**
 * Изменяет непосредственно отображаемую страницу
 */
function change_page() {
    // запоминаем старые данные о странице
    var old_now = NOW;
    var old_work_id = WORK_ID;

    // смотрим в историю
    var state = History.getState();
    NOW = state.data.now;
    WORK_ID = '';
    if (typeof state.data.work_id !== 'undefined' && parseInt(state.data.work_id) > 0) {
        WORK_ID = parseInt(state.data.work_id);
    }

    // если есть еще и WORK_ID - добавляем префиксы
    var work_id = ''; var work_now = '';
    if (WORK_ID !== '') {
        work_id = '/id/' + WORK_ID;
        work_now = '_id' + WORK_ID;
    }
    var old_work_now = '';
    if (old_work_id !== '') {
        old_work_now = '_id' + old_work_id;
    }

    // если старая и новая страница не совпадают - грузим новую
    if ((NOW !== old_now) || (NOW === 'work' && old_now === 'work' && WORK_ID !== old_work_id)) {
        page_changing = true;

        if ($('.b-menu_content').hasClass('b-menu_content_visible')) {
            main_menu_hide();
        }
        works_images_hover_off();

        // ******************************** если не список работ - берем аяксом данные и красиво появляем ****************************************
        if (NOW !== 'works') {

            // берем данные или если есть уже запрелоаденные
            get_ajax_data(NOW, work_id, work_now, function() {

                main_menu_update();

                // если есть на странице фоновое видео - устанавливаем его взамен картинок
                bg_video_set('wrap-' + NOW + work_now, function() {

                    // ********************* если входим в работу из списка работ - то тут вообще все по другому ***************************
                    if (NOW === 'work' && old_now === 'works') {

                        $('#wrap-' + NOW + work_now).css({ 'transition' : '' });
                        $('#wrap-' + NOW + work_now).css({ 'opacity' : 0 });

                        works_fly_to_id(WORK_ID, 'normal', function() {
                            $('#wrap-' + NOW + work_now).css({ 'transition' : 'opacity 300ms ease-out 200ms' });
                            $('#wrap-' + NOW + work_now).css({ 'top' : '', 'opacity' : '1', 'z-index' : 10 });

                            bg_img_onload();
                            bg_img_update();

                            setTimeout(function() {
                                $('#wrap-works').css({'top' : '100%'});
                                $('#wrap-' + NOW + work_now).css({ 'transition' : '', 'opacity' : '', 'z-index' : '' });
                                page_changing = false;
                                works_fly_back(false, function(){});

                                works_images_init();

                                top_panel_hide();
                                works_fontautosize();
                                works_images_hover();

                                // preload previous and next page
                                preloader_pages();
                            }, 550);
                        });

                        return true;
                    }
                    // **********************************************************************************************************************

                    bg_img_onload();
                    bg_img_update();
                    projects_onresize();
                    megaslider_destroy();

                    // слайдер в работах
                    if (NOW === 'work') { works_images_hover(); }

                    // запускаем слайдер
                    if (NOW === 'services') {
                        if (canPlaySvg) {
                            $('.b-services_slider_img_png').hide();
                            $('.b-services_slider_img_svg').css('display', '');
                        }
                        slider_services();
                    }

                    // может быть надо страничку выехать не снизу а сверху
                    if (typeof state.data.direction !== 'undefined' && state.data.direction !== '') {
                        if (state.data.direction === 'top') {
                            $('#wrap-' + NOW + work_now).css({'top' : '-100%'});
                        }
                    }

                    setTimeout(function(){
                        $('#wrap-' + NOW + work_now).css({'z-index' : 10, 'transition' : 'top 700ms cubic-bezier(0.215, 0.61, 0.355, 1)'});
                        $('#wrap-' + NOW + work_now).css({'top' : '0'});
                        setTimeout(function() {
                            $('#wrap-' + old_now + old_work_now).css({'top' : '100%'});
                            clean_ajax_data();

                            $('#wrap-' + NOW + work_now).css({'z-index' : '', 'transition' : ''});
                            page_changing = false;

                            if (NOW == 'work') { works_images_init(); }

                            top_panel_hide();
                            animate_pages();
                            works_fontautosize();

                            // preload previous and next page
                            preloader_pages();
                        }, 750);
                    }, 50);
                });
            });
        }
        // *************************** если список работ - данные уже взяты с самого начала, нужно их только показать ******************************
        else {
            main_menu_update();
            top_panel_hide();

            // если работа - тут другие эффекты
            if (old_now === 'work') {
                $('#wrap-works').css({ 'top' : '0' });
                works_fly_to_id(old_work_id, 'fast', function(){
                    //$('#wrap-' + old_now + old_work_now).css({ 'transition' : 'opacity 300ms ease-out' });
                    $('#wrap-' + old_now + old_work_now).css({ 'transition' : 'opacity 500ms ease-out' });
                    $('#wrap-' + old_now + old_work_now).css({ 'opacity' : '0' });
                    setTimeout(function(){
                        $('#wrap-' + old_now + old_work_now).css({ 'transition' : '', 'opacity' : '', 'top' : '100%', 'z-index' : '' });
                        clean_ajax_data();
                    //}, 500);
                    }, 700);

                    works_fly_back(true, function(){
                        page_changing = false;

                        // preload previous and next page
                        preloader_pages();
                    });
                });
            } else {
                works_fly_back(false, function(){
                    // может быть надо страничку выехать не снизу а сверху
                    if (typeof state.data.direction !== 'undefined' && state.data.direction !== '') {
                        if (state.data.direction === 'top') {
                            $('#wrap-' + NOW + work_now).css({'top' : '-100%'});
                        }
                    }

                    setTimeout(function(){
                        $('#wrap-' + NOW + work_now).css({'z-index' : 10, 'transition' : 'top 700ms cubic-bezier(0.215, 0.61, 0.355, 1)'});
                        $('#wrap-' + NOW + work_now).css({'top' : '0'});
                        setTimeout(function() {
                            $('#wrap-' + old_now + old_work_now).css({'top' : '100%'});
                            clean_ajax_data();

                            $('#wrap-' + NOW + work_now).css({'z-index' : '', 'transition' : ''});
                            page_changing = false;

                            // preload previous and next page
                            preloader_pages();
                        }, 750);
                    }, 50);
                });
            }
        }
        // ******************************************************************************************************************************************
    }
}

function clean_ajax_data() {
    var work_now = '';
    if (NOW === 'work' && WORK_ID !== '') {
        work_now = '_id' + WORK_ID;
    }

    $('.b-content_in').each(function(index, jqitem){
        if ($(jqitem).attr('id') != 'wrap-works' && $(jqitem).attr('id') !== 'wrap-' + NOW + work_now) {
            $(jqitem).html('');
        }
    });
}

function get_ajax_data(_now, _work_id, _work_now, callback) {
    if ($.trim($('#wrap-' + _now + _work_now).html()) == '') {
        $.ajax({
            type :'GET',
            url : baseUrl + '/api/get_' + _now + _work_id,
            timeout : 10000,
            error: function() {
                page_changing = false;
                if (typeof callback == 'function') { callback(); }
            },
            success : function(data) {
                $('#wrap-' + _now + _work_now).html(data);

                bg_img_onload();
                bg_img_update();

                if (typeof callback == 'function') { callback(); }
            }
        });
    } else {
        if (typeof callback == 'function') { callback(); }
    }
}

function works_fly_to_id(work_id, animation_type, callback)
{
    animation_type = (typeof animation_type === 'undefined') ? 'none' : animation_type;
    var anim_time_1 = 0;
    var anim_time_2 = 0;
    var anim_time_delay_2 = 0;

    switch (animation_type) {
        case 'fast':
            anim_time_1 = 50;
            anim_time_2 = 50;
            anim_time_delay_2 = 0; //anim_time_1 + 200;
            break;
        case 'normal':
            anim_time_1 = 700;
            anim_time_2 = 700;
            anim_time_delay_2 = 0; //anim_time_1 + 400;
            break;
    }

    $('#wrap-works .b-projects_wrap_in2').css({ '-webkit-transform' : '', '-webkit-transition' : '', 'transform' : '', 'transition' : '' });
    $('#wrap-works .b-projects_wrap').css({ '-webkit-transform' : '', '-webkit-transition' : '', 'transform' : '', 'transition' : '' });

    var imgW = $('#wrap-works .b-project_' + work_id).width();
    var imgH = $('#wrap-works .b-project_' + work_id).height();

    var contW = $('#wrap-works').width();
    var contH = $('#wrap-works').height();

    var scaleX = contW / imgW;
    var scaleY = contH / imgH;
    var scale = Math.max(scaleX, scaleY);

    var projectsScrollTop = $('#wrap-works .b-projects_wrap_in').scrollTop();

    var imgLeft = $('#wrap-works .b-project_' + work_id).offset().left;
    var imgTop = projectsScrollTop + $('#wrap-works .b-project_' + work_id).offset().top - $('.b-header_height').height();

    var transX = -imgLeft + contW/2 - imgW/2;
    var transY =  projectsScrollTop - imgTop + contH/2 - imgH/2;

    if (animation_type !== 'none') {
        $('#wrap-works .b-projects_wrap_in2').css({
            '-webkit-transition' : '-webkit-transform '+anim_time_1+'ms cubic-bezier(0.19, 1, 0.22, 1) ',
            'transition' : 'transform '+anim_time_1+'ms cubic-bezier(0.19, 1, 0.22, 1) '
        });
        $('#wrap-works .b-projects_wrap').css({
            '-webkit-transition' : '-webkit-transform '+anim_time_2+'ms cubic-bezier(0.19, 1, 0.22, 1) '+anim_time_delay_2+'ms',
            'transition' : 'transform '+anim_time_2+'ms cubic-bezier(0.19, 1, 0.22, 1) '+anim_time_delay_2+'ms'
        });
    }

    $('#wrap-works .b-project').removeClass('b-project_unhover');
    $('#wrap-works .b-project_' + work_id).addClass('b-project_unhover');

    $('#wrap-works .b-projects_wrap_in2').css({ '-webkit-transform' : 'translate3d('+transX+'px, '+transY+'px, 0)', 'transform' : 'translate3d('+transX+'px, '+transY+'px, 0)' });
    $('#wrap-works .b-projects_wrap').css({ '-webkit-transform' : 'scale('+scale+')', 'transform' : 'scale('+scale+')' });

    if (typeof callback === 'function') {
        if (animation_type !== 'none') {
            setTimeout(function(){
                $('#wrap-works .b-projects_wrap_in2').css({ '-webkit-transition' : '', 'transition' : '' });
                $('#wrap-works .b-projects_wrap').css({ '-webkit-transition' : '', 'transition' : '' });

                callback();
            }, (anim_time_2 + anim_time_delay_2 + 200));
        } else {
            callback();
        }
    }
}

function works_fly_back(animate, callback) {
    if (animate) {
        $('#wrap-works .b-projects_wrap').css({
            '-webkit-transition' : '-webkit-transform 500ms cubic-bezier(0.19, 1, 0.22, 1) 500ms',
            'transition' : 'transform 500ms cubic-bezier(0.19, 1, 0.22, 1) 500ms'
        });
        $('#wrap-works .b-projects_wrap_in2').css({
            '-webkit-transition' : '-webkit-transform 500ms cubic-bezier(0.19, 1, 0.22, 1) 1200ms',
            'transition' : 'transform 500ms cubic-bezier(0.19, 1, 0.22, 1) 1200ms'
        });
    }

    $('#wrap-works .b-projects_wrap_in2').css({ '-webkit-transform' : 'translate3d(0, 0, 0)', 'transform' : 'translate3d(0, 0, 0)' });
    $('#wrap-works .b-projects_wrap').css({ '-webkit-transform' : 'scale(1)', 'transform' : 'scale(1)' });

    if (typeof callback === 'function') {
        if (animate) {
            setTimeout(function(){
                $('#wrap-works .b-projects_wrap').css({ '-webkit-transition' : '', 'transition' : '' });
                $('#wrap-works .b-projects_wrap_in2').css({ '-webkit-transition' : '', 'transition' : '' });

                $('#wrap-works .b-project').removeClass('b-project_unhover');

                callback();
            }, 1850);
        } else {
            $('#wrap-works .b-project').removeClass('b-project_unhover');

            callback();
        }
    }
}

function works_get_all(callback) {
    $.ajax({
        type :'GET',
        url : baseUrl + '/api/get_works',
        timeout : 10000,
        success : function(data) {
            $('#wrap-works').html(data);
            projects_onresize();

            if (typeof callback === 'function') { callback(); }
        }
    });
}

/**
 * Существует ли страница с таким именем
 */
function pagesIdByName(page_name) {
    for (var i = 0; i < pages.length; i++) {
        if (pages[i]['name'] === page_name) {
            return i;
        }
    }
    return -1;
}