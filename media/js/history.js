/*********************************************** NAVIGATION *******************************************************/

/**
 * Запускается один раз после загрузки страницы
 * ищет, нет ли в истории или в адресе данных о странице, которую нужно загрузить
 * в противном случае загружает главную
 * Загружает не явно, а через History.pushState
 */
function history_load() {
    var state = History.getState();

    // в истории уже есть сохраненные данные о странице
    if (typeof state.data.now !== 'undefined') {
        history_change_state(false, true);
    }
    // данные о странице есть в адресе
    else if (typeof CURRENT_PAGE !== 'undefined' && CURRENT_PAGE !== '') {
        var pages_title = '';
        if (pagesIdByName(CURRENT_PAGE) > -1) {
            pages_title = pages[pagesIdByName(CURRENT_PAGE)]['title'];
        }

        if (CURRENT_PAGE == 'work' && typeof CURRENT_WORK_ID !== 'undefined' && CURRENT_WORK_ID !== '') {
            History.pushState({ 'now' : CURRENT_PAGE, work_id : parseInt(CURRENT_WORK_ID) }, pages_title + pages_title_suffix, baseUrl + '/work/id/' + CURRENT_WORK_ID);
        } else {
            History.pushState({ 'now' : CURRENT_PAGE }, pages_title + pages_title_suffix, baseUrl + '/' + CURRENT_PAGE);
        }
    }
    // вообще нигде никаких данных не найдено
    else {
        History.pushState({ 'now' : 'home' }, pages[pagesIdByName('home')]['title'] + pages_title_suffix, baseUrl + '/home');
    }
}

/**
 * То же, что и set_page, только для работы нужно установить id
 */
function set_work(work_id, direction) {
    if (!page_changing && parseInt(work_id) > 0) {
        var data_obj = {
            'now' : 'work',
            'work_id' : parseInt(work_id)
        };

        if (typeof direction !== 'undefined' && direction !== '') {
            data_obj.direction = direction;
        }

        History.pushState(data_obj, pages[pagesIdByName('work')]['title'] + pages_title_suffix, baseUrl + '/work/id/' + parseInt(work_id));
    }
}

/**
 * Обертка над History.pushState
 */
function set_page(page_name, direction) {
    if (!page_changing) {
        var data_obj = {
            'now' : page_name
        };

        if (typeof direction !== 'undefined' && direction !== '') {
            data_obj.direction = direction;
        }

        var pages_title = '';
        if (pagesIdByName(page_name) > -1) {
            pages_title = pages[pagesIdByName(page_name)]['title'];
        }

        History.pushState(data_obj, pages_title + pages_title_suffix, baseUrl + '/' + page_name);
    }
}

/**
 * Вызывается каждый раз при изменении состояния объекта History
 * является единственной точкой входа для change_page
 */
function history_change_state(event, from_start) {
    if (typeof from_start === 'undefined') {
        from_start = false;
    }

    var state = History.getState();

    // в прошлый раз страница могла приехать сверху, а после обновления должна снизу
    if (from_start) {
        if (typeof state.data.direction !== 'undefined' && state.data.direction !== '') {
            state.data.direction = 'bottom';
            History.replaceState(state.data, state.title, state.url);
        }
    }

    // Если такая страница существует вообще
    if (typeof state.data.now !== 'undefined' && pagesIdByName(state.data.now) > -1) {
        document.title = pages[pagesIdByName(state.data.now)]['title'] + pages_title_suffix;
        change_page();
    } else {
        History.replaceState({ 'now' : 'home' }, pages[pagesIdByName('home')]['title'] + pages_title_suffix, baseUrl + '/home');
    }
}