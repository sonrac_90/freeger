/******************************************* WORKS SLIDER ***************************************************/

var works_preview_time = 0;

function works_images_init() {
    if ($('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide img').length == 0) {
        if ($('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide').length > 0) {
            var image_slide = $('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide').eq(0);
            var img_html = '<img class="bg" src="'+image_slide.attr('data-src')+'" />';

            image_slide.html(img_html);
            bg_img_onload();
            bg_img_update();
        }
    }
}

function works_images_hover() {
    $('.b-works_button_pc').hover(works_images_hover_on, works_images_hover_off);
}

function works_images_slideshow() {
    if ($('#wrap-work_id'+WORK_ID+' .b-works_button_mobile').hasClass('b-works_button_hover')) {
        $('#wrap-work_id'+WORK_ID+' .b-works_button_mobile').removeClass('b-works_button_hover');
        works_images_hover_off(false);
    } else {
        $('#wrap-work_id'+WORK_ID+' .b-works_button_mobile').addClass('b-works_button_hover');
        works_images_hover_on(false, true);
    }
}

function works_images_hover_on(event, mobile) {
    mobile = (typeof mobile === 'undefined') ? false : mobile;

    $('.b-work_wrap').addClass('b-work_slideshow');
    if ($('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide').length > 1) {
        if (mobile) {
            works_image_change_mobile(works_image_get_current(), 0, false);
        } else {
            works_image_change(works_image_get_current(), 0, false);
        }

        clearTimeout(works_preview_time);
        works_preview_time = setTimeout(function() { works_preview_timeout(mobile); }, 3000);
        works_set_counter();
        works_image_preloader(WORK_ID);
    }
}

function works_images_hover_off(event) {
    clearTimeout(works_preview_time);
    preloader_clear();
    $('.b-work_wrap').removeClass('b-work_slideshow');
}

/*********************************************************************************************************************/

function works_preview_timeout(mobile) {
    if (mobile) {
        works_image_change_mobile(works_image_get_next(), works_image_get_current(), true);
    } else {
        works_image_change(works_image_get_next(), works_image_get_current(), true);
    }
    works_set_counter();

    works_preview_time = setTimeout(function() { works_preview_timeout(mobile); }, 3000);
}

function works_set_counter() {
    preloader_clear();
    $('.b-preloader').stop(true).animate({'width' : '100%'}, 3000, 'linear');
}

/*********************************************************************************************************************/

function works_image_get_current() {
    var curr_index = 0;
    $('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide').each(function(index, jqitem){
        if ($(jqitem).hasClass('image-slide-current')) {
            curr_index = index + 1;
        }
    });
    return curr_index;
}

function works_image_get_prev() {
    var curr_index = works_image_get_current();
    return (curr_index > 1) ? curr_index - 1 : $('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide img').length;
}

function works_image_get_next() {
    var curr_index = works_image_get_current();
    return ((curr_index + 1) <= $('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide img').length) ? curr_index + 1 : 1;
}

/*********************************************************************************************************************/

function works_image_change(next_index, curr_index, animate) {
    if (curr_index !== next_index) {
        $('#wrap-work_id'+WORK_ID+' .image-slide').removeClass('image-slide-current');
        $('#wrap-work_id'+WORK_ID+' .image-slide-'+next_index).addClass('image-slide-current');

        if (ieNum > 8) {
            if (animate) {
                $('#wrap-work_id'+WORK_ID+' .image-slide-'+curr_index).css({'z-index' : '-1'});
                $('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide-'+next_index).css({'z-index' : ''}).stop(true).animate({ 'opacity' : 1 }, 500, function(){
                    $('#wrap-work_id'+WORK_ID+' .b-work_slides_grey .image-slide-'+next_index).css({'z-index' : '', 'opacity' : 1});
                    $('#wrap-work_id'+WORK_ID+' .image-slide-'+curr_index).css({ 'opacity' : 0 });
                });
            } else {
                $('#wrap-work_id'+WORK_ID+' .image-slide').css({ 'opacity' : 0, 'z-index' : '-1', 'margin-left' : '' });
                $('#wrap-work_id'+WORK_ID+' .image-slide-'+next_index).css({ 'opacity' : 1, 'z-index' : '' });
            }
        } else {
            $('#wrap-work_id'+WORK_ID+' .image-slide-'+next_index).show();
            $('#wrap-work_id'+WORK_ID+' .image-slide-'+curr_index).hide();
        }
    }
}

function works_image_slide_left() {
    works_image_change_mobile(works_image_get_next(), works_image_get_current(), true, 'right');

    clearTimeout(works_preview_time);
    works_preview_time = setTimeout(function() { works_preview_timeout(true); }, 3000);
    works_set_counter();
}

function works_image_slide_right() {
    works_image_change_mobile(works_image_get_prev(), works_image_get_current(), true, 'left');

    clearTimeout(works_preview_time);
    works_preview_time = setTimeout(function() { works_preview_timeout(true); }, 3000);
    works_set_counter();
}

function works_image_change_mobile(next_index, curr_index, animate, direction) {
    if (curr_index !== next_index) {

        direction = (typeof direction === 'undefined') ? 'right' : direction;
        var margin_left = '100%';
        if (direction == 'left') {
            margin_left = '-100%';
        }

        $('#wrap-work_id'+WORK_ID+' .image-slide').removeClass('image-slide-current');
        $('#wrap-work_id'+WORK_ID+' .image-slide-'+next_index).addClass('image-slide-current');

        if (animate) {
            $('#wrap-work_id'+WORK_ID+' .image-slide-'+curr_index).css({'z-index' : '-1'});
            $('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide-'+next_index).css({'z-index' : '', 'margin-left' : margin_left}).stop(true).animate({ 'margin-left' : 0 }, 500, function(){
                $('#wrap-work_id'+WORK_ID+' .b-work_slides_grey .image-slide-'+next_index).css({'z-index' : '', 'margin-left' : 0});
                $('#wrap-work_id'+WORK_ID+' .image-slide-'+curr_index).css({ 'margin-left' : '100%' });
            });
        } else {
            $('#wrap-work_id'+WORK_ID+' .image-slide').css({ 'margin-left' : '100%', 'z-index' : '-1', 'opacity' : '' });
            $('#wrap-work_id'+WORK_ID+' .image-slide-'+next_index).css({ 'margin-left' : '', 'z-index' : '' });
        }
    }
}

/************************************************ PRELOADER ****************************************************/

function works_image_preloader(slides_work_id)
{
    function works_image_load_next() {
        if ($('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide.img-not-loaded').length > 0) {
            var img = new Image();
            img.onload = function(){ setTimeout(function(){ works_image_onload(); }, 2000); };
            img.src = $('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide.img-not-loaded').eq(0).attr('data-src');
        }
    }

    function works_image_onload() {
        if (slides_work_id == WORK_ID) {
            var works_curr_image = $('#wrap-work_id'+WORK_ID+' .b-work_slides .image-slide.img-not-loaded').eq(0);
            var works_curr_grey_image = $('#wrap-work_id'+WORK_ID+' .b-work_slides_grey .image-slide.img-not-loaded').eq(0);

            var img_html = '<img class="bg" src="'+works_curr_image.attr('data-src')+'" />';
            var img_grey_html = '<img class="bg" src="'+works_curr_grey_image.attr('data-src')+'" />';

            works_curr_image.html(img_html);
            works_curr_grey_image.html(img_grey_html);
            bg_img_onload();

            works_curr_image.removeClass('img-not-loaded');
            works_curr_grey_image.removeClass('img-not-loaded');

            bg_img_update();
            works_image_load_next();
        }
    }

    works_image_load_next();
}