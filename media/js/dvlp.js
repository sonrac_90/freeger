var isMobile = isUserAgentCheck(["iPhone", "iPad", "iPod", "android", "blackberry","CriOS","Mobile"]);
var isApple = isUserAgentCheck(["iPhone", "iPad", "iPod"]);
var ieNum = (function(){
                var undef,v = 3,div = document.createElement('div'),all = div.getElementsByTagName('i');
                while (div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',all[0]);
                return v > 4 ? v : 100500;
            }());
if (ieNum == 100500 && isUserAgentCheck(['Trident'])) { ieNum = 10; }

var isTouchSupported = 'ontouchstart' in window;

var canPlayVideo = false;
if (!isMobile && !isApple && ieNum > 8) {
    canPlayVideo = true;
}

var canPlaySvg = false;
if (ieNum > 8) {
    canPlaySvg = true;
}

function isUserAgentCheck(ua){
    var agent = navigator.userAgent;
    for(var i = 0; i < ua.length; i++)
        if(Boolean(agent.match(new RegExp(ua[i],"i"))))
            return true;
    return false;
}

// ********************** document classes ****************************

var htmlNode = document.html || document.getElementsByTagName('html')[0];
htmlNode.className = (isMobile ? 'html-mobile ' : '') +
                     (isApple ? 'html-apple ' : '') +
                     ((ieNum < 100500) ? 'html-ie html-ie'+ieNum+' ' : '') +
                     ((ieNum <= 8) ? 'html-ie_old ' : '');

/****************** CONSOLE LOG FIX IE *******************************/
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());