var _delta = 0;
var scroll_delta = 0;

function global_scroll_init()
{
    // главная
    $('#wrap-home').mousewheel(global_scroll_home);
    $('#wrap-home').touchscroll(global_scroll_home);

    // слайдер
    $('#wrap-services').mousewheel(global_scroll_services);
    $('#wrap-services').touchscroll(global_scroll_services);

    // работы
    $('#wrap-works').mousewheel(global_scroll_works_mouse);
    $('#wrap-works').touchscroll(global_scroll_works_touch, {'prevent_default' : false});

    // работа
    $('.wrap-work_id').each(function(index123, jqitem) {
        $(jqitem).mousewheel(function(event, delta, deltaX, deltaY) { global_scroll_inner_work(event, delta, deltaX, deltaY, index123); });
        $(jqitem).touchscroll(function(event, delta, deltaX, deltaY) { global_scroll_inner_work(event, delta, deltaX, deltaY, index123); });
    });

    // клиенты
    $('#wrap-awards').mousewheel(global_scroll_awards);
    $('#wrap-awards').touchscroll(global_scroll_awards);

    // контакты
    $('#wrap-contacts').mousewheel(global_scroll_contacts);
    $('#wrap-contacts').touchscroll(global_scroll_contacts);
}

function works_scroll_switch(delta, index) {
    var works_prev = works_scroll_get_prev(index);
    var works_next = works_scroll_get_next(index);

    if (works_prev !== '' && works_next !== '') {
        works_scroll(delta, works_prev, works_next);
    } else if (works_prev === '' && works_next !== '') {
        global_scroll(delta, 'works', '');
        works_scroll(delta, '', works_next);
    } else if (works_prev !== '' && works_next === '') {
        global_scroll(delta, '', 'awards');
        works_scroll(delta, works_prev, '');
    }
}

function works_scroll_get_prev(index) {
    if (index > 0) { return $('.wrap-work_id').eq(index - 1).attr('data-work_id'); }
    return '';
}

function works_scroll_get_next(index) {
    if ((index + 1) < $('.wrap-work_id').length) { return $('.wrap-work_id').eq(index + 1).attr('data-work_id'); }
    return '';
}

function works_scroll_get_current(index) {
    if ($('.wrap-work_id').eq(index).length > 0) { return $('.wrap-work_id').eq(index).attr('data-work_id'); }
    return '';
}

function works_scroll(delta, prev_work_id, next_work_id) {
    if (delta < 0 && scroll_delta > 0 || delta > 0 && scroll_delta < 0) {
        scroll_delta = 0;
    }
    scroll_delta += delta;

    if (scroll_delta < 0) {
        if (next_work_id !== '') {
            scroll_delta = 0;
            set_work(next_work_id);
        }
    } else if (scroll_delta > 0) {
        if (prev_work_id !== '') {
            scroll_delta = 0;
            set_work(prev_work_id, 'top');
        }
    }
}

function global_scroll(delta, prev_page, next_page) {
    if (delta < 0 && scroll_delta > 0 || delta > 0 && scroll_delta < 0) {
        scroll_delta = 0;
    }
    scroll_delta += delta;

    if (scroll_delta < 0) {
        if (next_page !== '') {
            scroll_delta = 0;
            set_page(next_page);
        }
    } else if (scroll_delta > 0) {
        if (prev_page !== '') {
            scroll_delta = 0;
            set_page(prev_page, 'top');
        }
    }
}

/************************************************************************************************************************************************/

function global_scroll_home(event, delta, deltaX, deltaY) {
    global_scroll(deltaY, '', 'services');
}

function global_scroll_services(event, delta, deltaX, deltaY) {
    var current_slide = parseInt($('.b-services_slider_button_active').attr('data-group_num'));
    var count_slides = $('.b-services_slider_button').length;

    if (deltaY < 0) {
        if (current_slide == count_slides) {
            if (!$('.b-services_slider').hasClass('js-slider_changing')) { global_scroll(deltaY, '', 'works'); }
        } else if (current_slide < count_slides) {
            //$('.b-services_slider_button_'+(current_slide+1)).click();
            if (typeof megaslider_change === 'function') {
                megaslider_change('next');
            }
        }
    } else if (deltaY > 0) {
        if (current_slide == 1) {
            if (!$('.b-services_slider').hasClass('js-slider_changing')) { global_scroll(deltaY, 'home', ''); }
        } else if (current_slide > 1) {
            //$('.b-services_slider_button_'+(current_slide-1)).click();
            if (typeof megaslider_change === 'function') {
                megaslider_change('prev');
            }
        }
    }
}

function global_scroll_works_mouse(event, delta, deltaX, deltaY) {
    if (delta < 0 && _delta > 0 || delta > 0 && _delta < 0) {
        _delta = 0;
    }
    _delta += delta;

    var projects_wrap_height = $('.b-projects_wrap_in').height();
    var projects_wrap_full_height = $('.b-projects_wrap_in2').height();
    var projects_scroll_top = $('.b-projects_wrap_in').scrollTop();

    if ((projects_wrap_full_height - projects_wrap_height) <= projects_scroll_top) {
        if (_delta < -5) {
            global_scroll(delta, '', 'awards');
        }
    } else if (projects_scroll_top <= 0) {
        if (_delta > 5) {
            global_scroll(delta, 'services', '');
        }
    } else {
        _delta = 0;
    }
}

function global_scroll_works_touch(event, delta, deltaX, deltaY) {
    var projects_wrap_height = $('.b-projects_wrap_in').height();
    var projects_wrap_full_height = $('.b-projects_wrap_in2').height();
    var projects_scroll_top = $('.b-projects_wrap_in').scrollTop();

    if ((projects_wrap_full_height - projects_wrap_height) <= projects_scroll_top) {
        if (deltaY < -2) {
            global_scroll(deltaY, '', 'awards');
        }
    } else if (projects_scroll_top <= 0) {
        if (deltaY > 2) {
            global_scroll(deltaY, 'services', '');
        }
    }
}

function global_scroll_inner_work(event, delta, deltaX, deltaY, index) {
    if ($('#wrap-work_id'+WORK_ID+' .b-work_wrap').hasClass('b-work_slideshow') && (deltaX != 0)) {
        if (deltaX < 0) {
            works_image_slide_left();
        } else if (deltaX > 0) {
            works_image_slide_right();
        }
    } else {
        works_scroll_switch(deltaY, index);
    }
}

function global_scroll_awards(event, delta, deltaX, deltaY) {
    global_scroll(deltaY, 'works', 'contacts');
}

function global_scroll_contacts(event, delta, deltaX, deltaY) {
    global_scroll(deltaY, 'awards', '');
}











