/************************************************* PRELOADER ******************************************************/

var curr_image_loaded = 0;

function preloader_init() {
    curr_image_loaded = 0;
    preloader_update();

    preloader_next_img();
}

function preloader_next_img() {
    var img = new Image();
    img.onload = preloader_img_onload;
    img.onerror = preloader_img_onload;
    img.src = baseUrl + '/media/img/' + preloader_data.images[curr_image_loaded]['path'];
}

function preloader_update() {
    $('.b-preloader').stop(true).animate({'width' : Math.ceil((curr_image_loaded / preloader_data.images.length) * 100)+'%'}, 200);
}

function preloader_clear() {
    $('.b-preloader').stop(true).width(0);
}

function preloader_img_onload() {
    curr_image_loaded++;
    preloader_update();

    if (curr_image_loaded < preloader_data.images.length) {
        preloader_next_img();
    }
    else {
        setTimeout(function() {
            preloader_clear();
            app_start();
        }, 500);
    }
}

/********************************************* PRELOADER PAGES *********************************************/

function preloader_pages() {
    switch (NOW) {
        case 'home':
            get_ajax_data('services', '', '');
        break;
        case 'services':
            get_ajax_data('home', '', '');
        break;
        case 'works':
            get_ajax_data('services', '', '', function(){
                get_ajax_data('awards', '', '');
            });
        break;
        case 'work':
            var index = -1;
            $('.wrap-work_id').each(function(index123, jqitem) {
                if ($(jqitem).attr('data-work_id') == WORK_ID) {
                    index = index123;
                    return false;
                }
            });

            if (parseInt(index) > -1) {
                var works_prev = works_scroll_get_prev(index);
                var works_next = works_scroll_get_next(index);

                if (works_prev !== '' && works_next !== '') {
                    get_ajax_data('work', '/id/'+works_next, '_id'+works_next, function(){
                        get_ajax_data('work', '/id/'+works_prev, '_id'+works_prev);
                    });
                } else if (works_prev === '' && works_next !== '') {
                    get_ajax_data('work', '/id/'+works_next, '_id'+works_next);
                } else if (works_prev !== '' && works_next === '') {
                    get_ajax_data('awards', '', '', function(){
                        get_ajax_data('work', '/id/'+works_prev, '_id'+works_prev);
                    });
                }
            }
        break;
        case 'awards':
            get_ajax_data('contacts', '', '');
        break;
        case 'contacts':
            get_ajax_data('awards', '', '');
        break;
    }
}