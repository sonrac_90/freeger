var NOW = 'preloader';
var WORK_ID = '';
var page_changing = false;
var pages_title_suffix = ' — ArOnce Creative';

var pages = [
    { 'title' : 'Home', 'name' : 'home', 'button' : 1 },
    { 'title' : 'Services', 'name' : 'services', 'button' : 2 },
    { 'title' : 'Works', 'name' : 'works', 'button' : 3 },
    { 'title' : 'Work', 'name' : 'work', 'button' : 3 },
    { 'title' : 'Clients & Awards', 'name' : 'awards', 'button' : 4 },
    { 'title' : 'Contacts', 'name' : 'contacts', 'button' : 5 }
];

var preloader_data = {
    images : [
        {"path":"bg\/home.jpg"},
        {"path":"bg\/services.jpg"},
        {"path":"bg\/clients.jpg"},
        {"path":"bg\/contacts.jpg"}
    ]
};

$(document).ready(function()
{
    /***************** RESIZERS ******************/
    $(window).resize(content_onresize);
    content_onresize();

    $(window).resize(bg_img_update);
    bg_img_onload();
    bg_img_update();

    $(window).resize(projects_onresize);
    projects_onresize();

    // orientation for mobile devices
    if (ieNum > 11) {
        updateOrientation();
        window.addEventListener('orientationchange', updateOrientation, false);
    }

    // скролл между страницами
    global_scroll_init();
    touch_scroll_init();

    // app start
    preloader_init();

    // top panel hide
    if (isMobile) {
        top_panel_hide();
        setInterval(function() { top_panel_hide(); }, 2000);
    }
});

/************************************************* COMMON *********************************************************/

/**
 * Старт приложения после прелоадера
 */
function app_start() {
    $('.b-header_height_in').animate({'top' : 0}, 700);

    works_get_all(function(){
        works_mouseup();

        History.Adapter.bind(window, "statechange", history_change_state);
        history_load();
    });
}

function updateOrientation() {
    switch (window.orientation) {
        case 0: case 180: //portrait
            $('html').removeClass('html-landscape').addClass('html-portrait');
        break;
        case 90: case -90: //landscape
            $('html').removeClass('html-portrait').addClass('html-landscape');
        break;
        default:
            $('html').removeClass('html-portrait').removeClass('html-landscape');
        break;
    }
    top_panel_hide();
    bg_img_update();
}

function top_panel_hide() {
    window.scrollTo(0, 0);
    //setTimeout(function(){ window.scrollTo(0, 1); }, 0);
    /*$('body,html').scrollTop(1000);
    $('.b-content_title').html($(window).scrollTop());*/
}

function slider_services() {
    megaslider_init({
        'slider_content_class' : 'b-services_slider_content',
        'slider_content_active_class' : 'b-services_slider_content_active',

        'slider_button_class' : 'b-services_slider_button',
        'slider_button_active_class' : 'b-services_slider_button_active',

        'pause_time' : 5000,
        'transition_time' : 1000,
        'transition_type' : ((ieNum <= 8) ? 'opacity' : 'margin-opacity')
    });
}

function bg_video_set(wrap_id, callback) {
    if ($('#'+wrap_id+' .b-content_video').length > 0 && canPlayVideo)
    {
        $('#'+wrap_id+' .b-content_video').show();
        $('#'+wrap_id+' .b-content_video_in').html('<video class="bg" r-width="1280" r-height="720" loop >' +
                                                        '<source src="'+$('#'+wrap_id+' .b-content_video_in').attr('data-src')+'.webm" type="video/webm">' +
                                                        //'<source src="'+$('#'+wrap_id+' .b-content_video_in').attr('data-src')+'.mp4" type="video/mp4">' +
                                                   '</video>');

        var video = $('#'+wrap_id+' .b-content_video_in video').get(0);
        if (typeof video !== 'undefined' && video.canPlayType) {
            setTimeout(function() { video.play(); }, 1000);
        }
    }

    if (typeof callback == 'function') {
        // ждем пока прогрузятся картинки чтобы не было миганий
        setTimeout(function() { callback(); }, 200);
    }
}

function animate_pages() {
    var state = History.getState();

    switch (NOW) {
        case 'home':
            $('.b-home_wrap').addClass('b-animate');
            $('.b-home_wrap .b-content_social').each(function(index, jqitem){
                setTimeout(function(){ $(jqitem).addClass('b-animate_social'); }, (index+1)*100);
            });
        break;
        case 'services':
            $('.b-services_slider').addClass('b-animate');
        break;
        case 'awards':
            $('.b-clients_wrap').addClass('b-animate');
        break;
        case 'contacts':
            $('.b-contacts_wrap').addClass('b-animate');
        break;
    }
}

/**
 * Предыдущие и следующие страницы относительно текущей
 * Нужно например для скролла или для прелоадера
 * @returns {{prev: {now: string, work_id: string}, next: {now: string, work_id: string}}}
 */
function pages_get_prevnext() {
    var page_prev = { 'now' : '', 'work_id' : '' };
    var page_next = { 'now' : '', 'work_id' : '' };

    switch (NOW) {
        case 'home':
            page_next.now = 'services';
        break;
        case 'services':
            page_prev.now = 'home';
            page_next.now = 'works';
        break;
        case 'works':
            page_prev.now = 'services';
            page_next.now = 'awards';
        break;
        case 'work':
            var index = -1;
            $('.wrap-work_id').each(function(index123, jqitem) {
                if ($(jqitem).attr('data-work_id') == WORK_ID) {
                    index = index123;
                    return false;
                }
            });

            if (parseInt(index) > -1) {
                var works_prev = works_scroll_get_prev(index);
                var works_next = works_scroll_get_next(index);

                if (works_prev !== '' && works_next !== '') {
                    page_prev.now = 'work'; page_prev.work_id = works_prev;
                    page_next.now = 'work'; page_next.work_id = works_next;
                }
                else if (works_prev === '' && works_next !== '') {
                    page_prev.now = 'works';
                    page_next.now = 'work'; page_next.work_id = works_next;
                }
                else if (works_prev !== '' && works_next === '') {
                    page_prev.now = 'work'; page_prev.work_id = works_prev;
                    page_next.now = 'awards';
                }
            }
        break;
        case 'awards':
            page_prev.now = 'works';
            page_next.now = 'contacts';
        break;
        case 'contacts':
            page_prev.now = 'awards';
        break;
    }

    return { 'prev' : page_prev, 'next' : page_next };
}

function works_mouseup() {
    if (!isMobile) {
        $('body').on('mouseup', '.b-work_wrap', function(e) {
            var container = $('.b-works_button');
            if (e.target != container[0] && !container.has(e.target).length) {
                set_page('works');
            }
        });
    }
}












































