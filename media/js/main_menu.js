/************************************************* MAIN MENU ******************************************************/

function main_menu_update() {
    for (var i = 0; i < pages.length; i++) {
        if (pages[i].name === NOW) {
            $('.b-menu_item').removeClass('b-menu_item_selected');
            $('.b-menu_pc .b-menu_item').eq((pages[i].button-1)).addClass('b-menu_item_selected');
            $('.b-menu_mobile .b-menu_item').eq((pages[i].button-1)).addClass('b-menu_item_selected');

            return true;
        }
    }
    return false;
}

function main_menu_autosize() {
    var window_width = $('body').width();
    var menu_button_width = 66 + $('.b-menu_button').width();
    var menu_width = window_width - menu_button_width;

    $('.b-menu_content').css({ 'width' : menu_width });

    if ($('.b-menu_content').hasClass('b-menu_content_visible')) {
        $('.b-menu_content').css({ 'left' : -menu_width });
        $('.b-main_content').css({ 'left' : -menu_width });
    }
}

function main_menu_toggle() {
    if ($('.b-menu_content').hasClass('b-menu_content_visible')) {
        main_menu_hide();
    } else {
        main_menu_show();
    }
}

function main_menu_show() {
    top_panel_hide();
    var menu_width = $('.b-menu_content').width();

    $('.b-menu_content').addClass('b-menu_content_visible');
    $('.b-main_content').stop(true).animate({'left' : -menu_width}, 500);
    $('.b-menu_content').stop(true).animate({'left' : -menu_width}, 500);
}

function main_menu_hide() {
    top_panel_hide();

    $('.b-main_content').stop(true).animate({'left' : 0}, 500);
    $('.b-menu_content').stop(true).animate({'left' : 0}, 500, function() {
        $('.b-menu_content').removeClass('b-menu_content_visible');
    });
}