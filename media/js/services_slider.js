/**
 *
 *  @preserve Copyright 2014
 *  Version: 0.2-fr
 *  Licensed under GPLv2.
 *  Usage: megaslider_init(options);
 *
 */

var megaslider_options = {};
var megaslider_changing = false;
var megaslider_interval = 0;

var megaslider_item;

/**************************************************** INIT SLIDER *********************************************************/

function megaslider_init(opt)
{
    // slider options
    megaslider_options = $.extend({
        slider_content_class: '',
        slider_content_active_class: '',

        slider_button_class: '',
        slider_button_active_class: '',

        slider_arrow_class: '',
        slider_arrow_left_class: '',
        slider_arrow_right_class: '',
        slider_arrow_disabled_class: '',

        'pause_time' : 5000,
        'transition_time' : 1000,
        'transition_type' : 'margin-opacity' // margin-opacity | opacity
    }, opt);

    // slider container
    megaslider_item = $('.b-services_slider').eq(0);

    // init slider
    megaslider_item.find('.'+megaslider_options.slider_content_class).each(function(idx, j_content){
        $(j_content).addClass(megaslider_options.slider_content_class+'_'+(idx+1)).attr('data-group_num', (idx+1));
        if (idx == 0) {
            $(j_content).addClass(megaslider_options.slider_content_active_class);
        } else {
            if (megaslider_options.transition_type == 'margin-opacity') {
                $(j_content).css({ 'margin-left' : '200%', 'opacity' : 0 });
            } else if (megaslider_options.transition_type == 'opacity') {
                $(j_content).css({ 'display' : 'none', 'opacity' : 0 });
            } else if (megaslider_options.transition_type == 'margin') {
                $(j_content).css({ 'margin-left' : '100%' });
            }
        }
    });

    // buttons
    if (megaslider_options.slider_button_class !== '') {
        megaslider_item.find('.'+megaslider_options.slider_button_class).each(function(idx, j_button){
            $(j_button).addClass(megaslider_options.slider_button_class+'_'+(idx+1)).attr('data-group_num', (idx+1));
            if (idx < $('.'+megaslider_options.slider_content_class).length) {
                $(j_button).click((function(index) {
                    return function() {
                        megaslider_change(index+1);
                    };
                })(idx));
            }
        });
    }

    // arrows
    if (megaslider_options.slider_arrow_left_class !== '') {
        megaslider_item.find('.'+megaslider_options.slider_arrow_left_class).click(function(){
            megaslider_change('prev');
        });
    }
    if (megaslider_options.slider_arrow_right_class !== '') {
        megaslider_item.find('.'+megaslider_options.slider_arrow_right_class).click(function(){
            megaslider_change('next');
        });
    }

    // slider interval
    megaslider_interval = setInterval(home_products_next, megaslider_options.pause_time);
}

function megaslider_destroy() {
    clearInterval(megaslider_interval);
    megaslider_changing = false;
}

/***************************************************** SLIDER FUNCTIONS **********************************************************************/

function megaslider_change(direction) {
    if (typeof direction !== 'undefined' && !megaslider_changing) {
        clearInterval(megaslider_interval);
        if (direction == 'prev') {
            home_products_prev();
        } else if (direction == 'next') {
            home_products_next();
        } else if (parseInt(direction) >= 0) {
            home_products_set(parseInt(direction));
        }
    }
}

function home_products_prev() {
    var curr_prod_num = 1;
    if (parseInt(megaslider_item.find('.'+megaslider_options.slider_content_active_class).attr('data-group_num')) > 0) {
        curr_prod_num = parseInt(megaslider_item.find('.'+megaslider_options.slider_content_active_class).attr('data-group_num'));
    }

    if (curr_prod_num > 1) {
        home_products_set(curr_prod_num-1);
    } else {
        home_products_set(megaslider_item.find('.'+megaslider_options.slider_content_class).length, 'left');
    }
}

function home_products_next() {
    var curr_prod_num = 1;
    if (parseInt(megaslider_item.find('.'+megaslider_options.slider_content_active_class).attr('data-group_num')) > 0) {
        curr_prod_num = parseInt(megaslider_item.find('.'+megaslider_options.slider_content_active_class).attr('data-group_num'));
    }

    if (curr_prod_num < megaslider_item.find('.'+megaslider_options.slider_content_class).length) {
        home_products_set(curr_prod_num+1);
    } else {
        home_products_set(1, 'right');
    }
}

function home_products_set(prod_num, direction) {
    direction = direction || '';

    var old_prod_num = 1;
    if (parseInt(megaslider_item.find('.'+megaslider_options.slider_content_active_class).attr('data-group_num')) > 0) {
        old_prod_num = parseInt(megaslider_item.find('.'+megaslider_options.slider_content_active_class).attr('data-group_num'));
    }

    if (old_prod_num != prod_num)
    {
        megaslider_changing = true;
        megaslider_item.addClass('js-megaslider_changing');

        var old_end_position = 0;
        var new_start_position = 0;

        if (megaslider_options.transition_type == 'margin-opacity')
        {
            if (direction !== '') {
                if (direction === 'left') {
                    old_end_position = '200%';
                    new_start_position = '-200%';
                } else if (direction === 'right') {
                    old_end_position = '-200%';
                    new_start_position = '200%';
                }
            } else {
                if (old_prod_num < prod_num) {
                    old_end_position = '-200%';
                    new_start_position = '200%';
                } else if (prod_num < old_prod_num) {
                    old_end_position = '200%';
                    new_start_position = '-200%';
                }
            }

            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).css({ 'margin-left' : new_start_position });

            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+old_prod_num).css({
                '-webkit-transition' : 'opacity '+Math.floor(megaslider_options.transition_time/2)+'ms ease-out',
                '-moz-transition' : 'opacity '+Math.floor(megaslider_options.transition_time/2)+'ms ease-out',
                'transition' : 'opacity '+Math.floor(megaslider_options.transition_time/2)+'ms ease-out'
            });
            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+old_prod_num).css({ 'opacity' : 0 });
            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+old_prod_num).animate({ 'margin-left' : old_end_position }, megaslider_options.transition_time);

            setTimeout(function() {
                megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).animate({ 'margin-left' : '0' }, megaslider_options.transition_time, function() {
                    megaslider_changing = false;
                    megaslider_item.removeClass('js-megaslider_changing');
                });

                megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).css({
                    '-webkit-transition' : 'opacity '+Math.floor(megaslider_options.transition_time/2)+'ms ease-out '+Math.floor(megaslider_options.transition_time/2)+'ms',
                    '-moz-transition' : 'opacity '+Math.floor(megaslider_options.transition_time/2)+'ms ease-out '+Math.floor(megaslider_options.transition_time/2)+'ms',
                    'transition' : 'opacity '+Math.floor(megaslider_options.transition_time/2)+'ms ease-out '+Math.floor(megaslider_options.transition_time/2)+'ms'
                });
                megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).css({ 'opacity' : 1 });
            }, 50);
        }
        else if (megaslider_options.transition_type == 'opacity')
        {
            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+old_prod_num).css({
                '-webkit-transition' : 'opacity '+megaslider_options.transition_time+'ms ease-out',
                '-moz-transition' : 'opacity '+megaslider_options.transition_time+'ms ease-out',
                'transition' : 'opacity '+megaslider_options.transition_time+'ms ease-out'
            });
            setTimeout(function() { megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+old_prod_num).css({ 'opacity' : 0 }); }, 30);

            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).show();
            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).css({
                '-webkit-transition' : 'opacity '+megaslider_options.transition_time+'ms ease-out',
                '-moz-transition' : 'opacity '+megaslider_options.transition_time+'ms ease-out',
                'transition' : 'opacity '+megaslider_options.transition_time+'ms ease-out'
            });
            setTimeout(function() { megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).css({ 'opacity' : 1 }); }, 30);

            setTimeout(function()
            {
                megaslider_changing = false;
                megaslider_item.removeClass('js-megaslider_changing');
                megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+old_prod_num).hide();

            }, (megaslider_options.transition_time + 30));
        }
        else if (megaslider_options.transition_type == 'margin')
        {
            if (direction !== '') {
                if (direction === 'left') {
                    old_end_position = '100%';
                    new_start_position = '-100%';
                } else if (direction === 'right') {
                    old_end_position = '-100%';
                    new_start_position = '100%';
                }
            } else {
                if (old_prod_num < prod_num) {
                    old_end_position = '-100%';
                    new_start_position = '100%';
                } else if (prod_num < old_prod_num) {
                    old_end_position = '100%';
                    new_start_position = '-100%';
                }
            }

            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).css({ 'margin-left' : new_start_position });
            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+old_prod_num).animate({ 'margin-left' : old_end_position }, megaslider_options.transition_time);
            megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).animate({ 'margin-left' : '0' }, megaslider_options.transition_time, function () {
                megaslider_changing = false;
                megaslider_item.removeClass('js-megaslider_changing');
            });
        }

        megaslider_item.find('.'+megaslider_options.slider_content_class).removeClass(megaslider_options.slider_content_active_class);
        megaslider_item.find('.'+megaslider_options.slider_content_class+'_'+prod_num).addClass(megaslider_options.slider_content_active_class);

        megaslider_item.find('.'+megaslider_options.slider_button_class).removeClass(megaslider_options.slider_button_active_class);
        if (megaslider_item.find('.'+megaslider_options.slider_button_class+'[data-group_num='+prod_num+']').length > 0) {
            megaslider_item.find('.'+megaslider_options.slider_button_class+'[data-group_num='+prod_num+']').addClass(megaslider_options.slider_button_active_class);
        }
    }
}







