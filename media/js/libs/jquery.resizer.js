/**
 *
 *  @preserve Copyright 2013
 *  Version: 0.21
 *  Licensed under GPLv2.
 *  Usage: $('.bg').resizer(options);
 *
 */
(function($){
    $.fn.resizer = function(options)
    {
        var j_image = this;

        options = $.extend({
            max_scale: 100,
            scale_mod: 'crop', // crop | fit
            percents : false
        }, options);

        return j_image.each(function(index, jqitem)
        {
            var j_container = $(jqitem).parent();
            var j_image = $(jqitem);




            j_image.css({
                'position':'absolute',
                'top': '',
                'left': '',
                'width': '',
                'height': ''
            });




            var containerW = j_container.width();
            var containerH = j_container.height();

            var imageW = (typeof j_image.attr('r-width') !== 'undefined') ? parseInt(j_image.attr('r-width')) : j_image.width();
            var imageH = (typeof j_image.attr('r-height') !== 'undefined') ? parseInt(j_image.attr('r-height')) : j_image.height();

            var scale_Coef_X = Math.min(options.max_scale, Math.floor(containerW) / imageW);
            var scale_Coef_Y = Math.min(options.max_scale, Math.floor(containerH) / imageH);




            if (options.scale_mod == 'fit') {
                scale_Coef_X = Math.min(scale_Coef_X, scale_Coef_Y);
                scale_Coef_Y = Math.min(scale_Coef_X, scale_Coef_Y);
            } else if (options.scale_mod == 'crop') {
                scale_Coef_X = Math.max(scale_Coef_X, scale_Coef_Y);
                scale_Coef_Y = Math.max(scale_Coef_X, scale_Coef_Y);
            }

            imageW = imageW * scale_Coef_X;
            imageH = imageH * scale_Coef_Y;




            var c_left = 0;
            // Выровнять по правому краю
            if (j_image.hasClass('bg-right')) { c_left = Math.floor((containerW - imageW)); }
            // по левому краю
            else if (j_image.hasClass('bg-left')) { c_left = 0; }
            // по центру (по умолчанию)
            else { c_left = Math.floor((containerW - imageW) / 2); }




            var c_top = 0;
            // Выровнять по низу
            if (j_image.hasClass('bg-bottom')) { c_top = Math.floor((containerH - imageH)); }
            // по верху
            else if (j_image.hasClass('bg-top')) { c_top = 0; }
            // по центру (по умолчанию)
            else { c_top = Math.floor((containerH - imageH) / 2); }




            if (j_container.css('position') != 'absolute') {
                j_container.css({'position' : 'relative'});
            }



            if (!options.percents) {
                j_image.css({
                    'position':'absolute',
                    'top': c_top,
                    'left': c_left,
                    'width':imageW,
                    'height':imageH
                });
            } else {
                j_image.css({
                    'position':'absolute',
                    'top': ((c_top / containerW) * 100) + '%',
                    'left': ((c_left / containerH) * 100) + '%',
                    'width': ((imageW / containerW) * 100) + '%',
                    'height': ((imageH / containerH) * 100) + '%'
                });
            }
        });
    };
})(jQuery);