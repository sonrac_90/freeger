/**
 *
 *  @preserve Copyright 2013
 *  Version: 0.1.1
 *  Licensed under GPLv2.
 *  Usage: $('div').mapScroll(options);
 *
 */
(function($){
    $.fn.mapScroll = function(options)
    {
        var isTouchSupported = 'ontouchstart' in window;
        var settings = $.extend({

        }, options);

        return this.each(function(index, jqitem)
        {
            var j_item = $(jqitem);
            var j_drag = false;
            var j_start_left_css = 0;
            var j_start_left_mouse = 0;
            var j_start_top_css = 0;
            var j_start_top_mouse = 0;

            /***************************************************************************************************/

            j_item.mousedown(function(e) {
                if (!j_drag) {
                    j_drag = true;
                    j_start_left_mouse = e.pageX;
                    j_start_left_css = parseInt($(j_item).css('left'));
                    j_start_top_mouse = e.pageY;
                    j_start_top_css = parseInt($(j_item).css('top'));
                }
                return false;
            });

            j_item.mousemove(function(e) {
                if (j_drag) {
                    var left_new = e.pageX - j_start_left_mouse;
                    var top_new = e.pageY - j_start_top_mouse;
                    j_item.css({
                        'left' : Math.max(Math.min(0, (j_item.parent().width() - j_item.width())), Math.min(0, (j_start_left_css + left_new))),
                        'top' : Math.max(Math.min(0, (j_item.parent().height() - j_item.height())), Math.min(0, (j_start_top_css + top_new)))
                    });
                }
                return false;
            });

            j_item.mousewheel(function (event, delta, deltaX, deltaY) {
                var top_new = parseInt($(j_item).css('top')) + delta * 20;
                j_item.css({
                    'top' : Math.max(Math.min(0, (j_item.parent().height() - j_item.height())), Math.min(0, top_new))
                });
            });

            $('body').mouseup(function(e) {
                if (j_drag) {
                    j_drag = false;
                }
            });

            $(window).resize(function() {
                j_item.css({
                    'left' : 0,
                    'top' : 0
                });
            });

            /***************************************************************************************************/

            if (isTouchSupported)
            {
                jqitem.addEventListener('touchstart', function(e) {

                    if (!j_drag) {
                        j_drag = true;
                        var touch = e.changedTouches[0];

                        j_start_left_mouse = touch.clientX;
                        j_start_top_mouse = touch.clientY;

                        j_start_left_css = parseInt($(j_item).css('left'));
                        j_start_top_css = parseInt($(j_item).css('top'));
                    }

                }, false);

                jqitem.addEventListener('touchmove', function(e) {
                    e.preventDefault();

                    if (j_drag) {
                        var touch = e.changedTouches[0];

                        var left_new = touch.clientX - j_start_left_mouse;
                        var top_new = touch.clientY - j_start_top_mouse;
                        j_item.css({
                            'left' : Math.max((j_item.parent().width() - j_item.width()), Math.min(0, (j_start_left_css + left_new))),
                            'top' : Math.max((j_item.parent().height() - j_item.height()), Math.min(0, (j_start_top_css + top_new)))
                        });
                    }
                    return false;
                }, false);

                jqitem.addEventListener('touchend', function() {
                    if (j_drag) {
                        j_drag = false;
                    }
                }, false);
            }

            /***************************************************************************************************/
        });
    };
})(jQuery);