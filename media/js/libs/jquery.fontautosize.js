/**
 *
 *  @preserve Copyright 2014
 *  Version: 0.1
 *  Licensed under GPLv2.
 *  Usage: $('.container span').fontautosize(options);
 *
 */
(function($){
    $.fn.fontautosize = function(opt)
    {
        var options = $.extend({
            max_font_size: 240 // px
        }, opt);

        return this.each(function(index, jqitem)
        {
            var j_item = $(jqitem);
            var j_container = $(jqitem).parent();
            var current_font_size = 16;

            j_item.css({ 'font-size' : current_font_size + 'px' });
            j_container.css({ 'height' : '' });

            var current_width = j_item.width();
            var max_width = j_container.width();
            var width_koef = max_width / current_width;

            j_item.css({
                'font-size' : Math.min(options.max_font_size, (Math.floor(current_font_size * width_koef) - 10)) + 'px'
            });
            j_container.css({
                'height' : Math.min(options.max_font_size, (Math.floor(current_font_size * width_koef) - 10)) + 'px'
            });
        });
    };
})(jQuery);