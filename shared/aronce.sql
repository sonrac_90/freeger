/*
Navicat MySQL Data Transfer

Source Server         : dvlp.cc
Source Server Version : 50531
Source Host           : dvlp.cc:3306
Source Database       : freeger

Target Server Type    : MYSQL
Target Server Version : 50531
File Encoding         : 65001

Date: 2014-04-29 19:49:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dvlp_contacts
-- ----------------------------
DROP TABLE IF EXISTS `dvlp_contacts`;
CREATE TABLE `dvlp_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `position` int(5) NOT NULL,
  `visible` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dvlp_contacts
-- ----------------------------
INSERT INTO `dvlp_contacts` VALUES ('12', 'Russia', '26&nbsp;Volgogradskiy&nbsp;Ave, Office&nbsp;1014,&nbsp;Moscow', '+7 (495) 799-59-96', '<a href=\"mailto:hello@freeger.com\">hello@freeger.com</a>', '1', '0');
INSERT INTO `dvlp_contacts` VALUES ('13', 'United States', '650&nbsp;Lighthouse&nbsp;Ave,&nbsp;Suite&nbsp;235, Pacific&nbsp;Grove,&nbsp;CA&nbsp;93950', '+1 (831) 324-06-41', '<a href=\"mailto:us@freeger.com\">us@freeger.com</a>', '2', '0');

-- ----------------------------
-- Table structure for dvlp_home
-- ----------------------------
DROP TABLE IF EXISTS `dvlp_home`;
CREATE TABLE `dvlp_home` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Title Main Page',
  `text` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Text Page',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Info Main Page';

-- ----------------------------
-- Records of dvlp_home
-- ----------------------------
INSERT INTO `dvlp_home` VALUES ('1', 'Digital Miracles', 0x4672656567657220697320616E2061776172642D77696E6E696E6720687962726964206469676974616C206167656E6379207769746820485120696E204C6F7320416E67656C65732026206F666669636520696E204D6F73636F772E2053696E6365206F757220666F756E646174696F6E20696E2032303037206F757220676F616C20686173206265656E20746F20757365206469676974616C20746563686E6F6C6F677920746F2063726561746520657870657269656E636573207769746820612064656570657220696D706163742E203C7370616E3E4F7572207465616D7320617265206D616465207570206F66206D756C74692D74616C656E746564206372656174697665732C20746563686E6F6C6F6769737420616E642070726F6475636572732C2070617373696F6E6174656C7920637572696F75732061626F7574207573696E6720746563686E6F6C6F677920696E206E657720776179732E205765206C6F766520646F696E67207468696E6773207468617420686176656EE2809974206265656E20646F6E65206265666F726520616E6420696E20706172746E6572736869702077697468206F757220706172746E6572732077652063616E206F66666572206120756E697175652077617920746F206272696467652074686520676170206265747765656E20636F6E74656E7420616E6420746563686E6F6C6F67792C206265747765656E206E617272617469766520616E6420696E74657261637469766520657870657269656E63652E3C2F7370616E3E);

-- ----------------------------
-- Table structure for dvlp_img_works
-- ----------------------------
DROP TABLE IF EXISTS `dvlp_img_works`;
CREATE TABLE `dvlp_img_works` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `works_id` int(2) NOT NULL,
  `position` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dvlp_img_works
-- ----------------------------
INSERT INTO `dvlp_img_works` VALUES ('294', '13', '1', '1.jpg');
INSERT INTO `dvlp_img_works` VALUES ('295', '13', '2', '295.jpg');
INSERT INTO `dvlp_img_works` VALUES ('296', '13', '3', '296.jpg');
INSERT INTO `dvlp_img_works` VALUES ('297', '12', '1', '297.jpg');
INSERT INTO `dvlp_img_works` VALUES ('298', '12', '2', '298.jpg');
INSERT INTO `dvlp_img_works` VALUES ('299', '14', '1', '299.jpg');
INSERT INTO `dvlp_img_works` VALUES ('300', '20', '1', '300.jpg');
INSERT INTO `dvlp_img_works` VALUES ('301', '19', '1', '301.jpg');
INSERT INTO `dvlp_img_works` VALUES ('302', '35', '1', '302.jpg');
INSERT INTO `dvlp_img_works` VALUES ('303', '36', '1', '303.jpg');
INSERT INTO `dvlp_img_works` VALUES ('304', '37', '1', '304.jpg');
INSERT INTO `dvlp_img_works` VALUES ('305', '39', '1', '305.jpg');
INSERT INTO `dvlp_img_works` VALUES ('306', '41', '1', '306.jpg');
INSERT INTO `dvlp_img_works` VALUES ('307', '42', '1', '307.jpg');
INSERT INTO `dvlp_img_works` VALUES ('308', '44', '1', '308.jpg');
INSERT INTO `dvlp_img_works` VALUES ('309', '45', '1', '309.jpg');
INSERT INTO `dvlp_img_works` VALUES ('310', '46', '1', '310.jpg');
INSERT INTO `dvlp_img_works` VALUES ('311', '47', '1', '311.jpg');
INSERT INTO `dvlp_img_works` VALUES ('312', '48', '1', '312.jpg');
INSERT INTO `dvlp_img_works` VALUES ('313', '49', '1', '313.jpg');
INSERT INTO `dvlp_img_works` VALUES ('314', '50', '1', '314.jpg');
INSERT INTO `dvlp_img_works` VALUES ('315', '51', '1', '315.jpg');
INSERT INTO `dvlp_img_works` VALUES ('316', '53', '1', '316.jpg');
INSERT INTO `dvlp_img_works` VALUES ('317', '54', '1', '317.jpg');
INSERT INTO `dvlp_img_works` VALUES ('318', '55', '1', '318.jpg');
INSERT INTO `dvlp_img_works` VALUES ('319', '56', '1', '319.jpg');
INSERT INTO `dvlp_img_works` VALUES ('320', '62', '1', '320.jpg');
INSERT INTO `dvlp_img_works` VALUES ('321', '63', '1', '321.jpg');
INSERT INTO `dvlp_img_works` VALUES ('322', '65', '1', '322.jpg');
INSERT INTO `dvlp_img_works` VALUES ('323', '66', '1', '323.jpg');
INSERT INTO `dvlp_img_works` VALUES ('324', '67', '1', '324.jpg');
INSERT INTO `dvlp_img_works` VALUES ('325', '68', '1', '325.jpg');
INSERT INTO `dvlp_img_works` VALUES ('326', '68', '2', '326.jpg');
INSERT INTO `dvlp_img_works` VALUES ('327', '69', '1', '327.jpg');
INSERT INTO `dvlp_img_works` VALUES ('328', '69', '2', '328.jpg');
INSERT INTO `dvlp_img_works` VALUES ('329', '70', '1', '329.jpg');
INSERT INTO `dvlp_img_works` VALUES ('330', '70', '2', '330.jpg');
INSERT INTO `dvlp_img_works` VALUES ('331', '12', '0', '331.jpg');
INSERT INTO `dvlp_img_works` VALUES ('332', '71', '1', '332.jpg');
INSERT INTO `dvlp_img_works` VALUES ('333', '71', '2', '333.jpg');
INSERT INTO `dvlp_img_works` VALUES ('334', '72', '1', '334.jpg');
INSERT INTO `dvlp_img_works` VALUES ('335', '72', '2', '335.jpg');
INSERT INTO `dvlp_img_works` VALUES ('336', '73', '1', '336.jpg');
INSERT INTO `dvlp_img_works` VALUES ('337', '73', '2', '337.jpg');
INSERT INTO `dvlp_img_works` VALUES ('338', '74', '1', '338.jpg');
INSERT INTO `dvlp_img_works` VALUES ('339', '75', '1', '339.jpg');
INSERT INTO `dvlp_img_works` VALUES ('340', '76', '1', '340.jpg');

-- ----------------------------
-- Table structure for dvlp_services
-- ----------------------------
DROP TABLE IF EXISTS `dvlp_services`;
CREATE TABLE `dvlp_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'About Services',
  `icons_png` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Icon PNG',
  `icons_svg` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Icon SVG',
  `position` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `visible` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dvlp_services
-- ----------------------------
INSERT INTO `dvlp_services` VALUES ('12', 'We believe that a great idea needs to be visualised and communicated and that some of the biggest learnings come through doing and not just talking. We believe that a great idea not just talking.', 'slider-img104fc4702469c75db5ccf27dc9ce6515.png', 'mouse2ef7c678e531c0911a11ee53702af39d4.svg', '0', 'Creative & Strategy', '1');
INSERT INTO `dvlp_services` VALUES ('13', 'We believe that a great idea needs to be visualised and communicated and that some of the biggest learnings a nee me of the biggest learnings come through doing and not just talking.', 'slider-img006fff5bbbcb90c138299e40b3e97e93.png', 'mouse248f9cc3966f2875783d600bfd5c2bef1.svg', '1', 'Strategy & Creative', '1');
INSERT INTO `dvlp_services` VALUES ('15', 'We believe that a great idea needs to be visualised and communicated and that some of the biggest learnings come through doing and not just talking. We believe that a great idea not just talking.', 'slider-imgbecdcc6716f3e550155127a8dcad092c.png', 'mouse266500641bf8ee48b6151f8cda0bdb038.svg', '2', 'Strategy & Strategy', '1');

-- ----------------------------
-- Table structure for dvlp_social
-- ----------------------------
DROP TABLE IF EXISTS `dvlp_social`;
CREATE TABLE `dvlp_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` int(5) NOT NULL,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Social Network Link';

-- ----------------------------
-- Records of dvlp_social
-- ----------------------------
INSERT INTO `dvlp_social` VALUES ('1', '2', '13929954099.jpg', 'http://www.twitter.com/freegeragency', 'Twitter');
INSERT INTO `dvlp_social` VALUES ('2', '1', '13929955300.jpg', 'http://vk.com/public54747478', 'Vkontakte');
INSERT INTO `dvlp_social` VALUES ('3', '0', '', 'http://www.facebook.com/pages/Freeger-Digital-CG-Production/138057416255869  ', 'Facebook');
INSERT INTO `dvlp_social` VALUES ('4', '3', '', 'http://vimeo.com/user1397011', 'Vimeo');

-- ----------------------------
-- Table structure for dvlp_user
-- ----------------------------
DROP TABLE IF EXISTS `dvlp_user`;
CREATE TABLE `dvlp_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Username',
  `pass` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Password',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'E-Mail',
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Users table';

-- ----------------------------
-- Records of dvlp_user
-- ----------------------------
INSERT INTO `dvlp_user` VALUES ('1', 'admin', 'c4ca4238a0b923820dcc509a6f75849b', 'admin@freeger.com', 'admin');

-- ----------------------------
-- Table structure for dvlp_works
-- ----------------------------
DROP TABLE IF EXISTS `dvlp_works`;
CREATE TABLE `dvlp_works` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `link` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `visible` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dvlp_works
-- ----------------------------
INSERT INTO `dvlp_works` VALUES ('71', 'Beeline 4G', 'Promosite for mobile operator', 'http://www.freeger.com/projects/beeline4g/', '1', '1');
INSERT INTO `dvlp_works` VALUES ('72', 'Gravity', '3D Works for HTML5 WebGL movie site / Developed for B-reel agency', 'http://gravitymovie.warnerbros.com/', '2', '1');
INSERT INTO `dvlp_works` VALUES ('73', ' Stalingrad', 'Promo site for russian movie', 'http://stalingrad-film.ru/', '3', '1');
INSERT INTO `dvlp_works` VALUES ('74', 'Jim Carrey', 'Official site of famous actor / Developed for 65media agency', 'http://jimcarrey.com/', '4', '1');
INSERT INTO `dvlp_works` VALUES ('75', 'Megafon Circus', 'Promo site for Megafon / Developed for Deluxe Interactive agency', 'http://zarkana.megafon.ru/cirque/', '5', '1');
INSERT INTO `dvlp_works` VALUES ('76', 'ContextAd', 'Promo tour for Contad', 'http://www.freeger.com/projects/contextad/', '6', '1');
